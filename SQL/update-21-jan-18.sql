-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2018 at 08:01 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sibanjar`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `nik` char(16) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `hak_akses` tinyint(4) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`nik`, `nama`, `password`, `hak_akses`, `photo`, `token`) VALUES
('5103060609980005', 'I Putu Agus Wahyu Widiatmika', '5350e4ecdf6983acfe8ae7ca197593e9', 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_banjar`
--

CREATE TABLE `tb_banjar` (
  `id` char(15) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` text,
  `alamat` text NOT NULL,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_banjar`
--

INSERT INTO `tb_banjar` (`id`, `nama`, `deskripsi`, `alamat`, `gambar`) VALUES
('BJR-18012100001', 'Tegal Luwih', 'Ini sebuah banjar', 'Tegal Luwih', 'uploads/img/e628c4f4666c4cd80161c0db8bd7e97c.jpg');

--
-- Triggers `tb_banjar`
--
DELIMITER $$
CREATE TRIGGER `delete_banjar` BEFORE DELETE ON `tb_banjar` FOR EACH ROW BEGIN
  DELETE FROM tb_kaka WHERE id_banjar = OLD.id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_cat_gatra`
--

CREATE TABLE `tb_cat_gatra` (
  `id` char(15) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_cat_lapor`
--

CREATE TABLE `tb_cat_lapor` (
  `id` char(15) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cat_lapor`
--

INSERT INTO `tb_cat_lapor` (`id`, `judul`, `deskripsi`) VALUES
('CLP-18012200001', 'Kebakaran', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_gatra`
--

CREATE TABLE `tb_gatra` (
  `id` char(15) NOT NULL,
  `nik` char(16) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `konten` text NOT NULL,
  `id_cat_gatra` char(15) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kaka`
--

CREATE TABLE `tb_kaka` (
  `nokk` char(16) NOT NULL,
  `id_kepala` varchar(16) NOT NULL,
  `id_banjar` char(16) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kaka`
--

INSERT INTO `tb_kaka` (`nokk`, `id_kepala`, `id_banjar`, `alamat`) VALUES
('5102223441155221', '5102223445555224', 'BJR-18012100001', 'B'),
('5102223443355221', '5102223445555228', 'BJR-18012100001', 'D'),
('5102223445225221', '5102223445555227', 'BJR-18012100001', 'C'),
('5102223445555221', '5102223445555225', 'BJR-18012100001', 'E'),
('5102223445665221', '5102223445555221', 'BJR-18012100001', 'A');

--
-- Triggers `tb_kaka`
--
DELIMITER $$
CREATE TRIGGER `delete_kaka` BEFORE DELETE ON `tb_kaka` FOR EACH ROW BEGIN
  DELETE FROM tb_user WHERE nokk = OLD.nokk;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_lapor`
--

CREATE TABLE `tb_lapor` (
  `id` char(15) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `id_cat_lapor` char(15) NOT NULL,
  `nik` char(16) NOT NULL,
  `status` enum('ditanggapi','belum ditanggapi') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_lapor`
--

INSERT INTO `tb_lapor` (`id`, `judul`, `deskripsi`, `gambar`, `id_cat_lapor`, `nik`, `status`) VALUES
('LPR-18012200001', 'Kleng', 'aaaaaaaaaaa', 'uploads/img/3fdca70940cc1c135a50329e06487400.jpg', 'CLP-18012200001', '5102223445555221', 'belum ditanggapi');

-- --------------------------------------------------------

--
-- Table structure for table `tb_lingang`
--

CREATE TABLE `tb_lingang` (
  `id` char(16) NOT NULL,
  `nik` char(16) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `deskripsi` text,
  `tgl_awal` date NOT NULL,
  `tgl_akhir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_lingang`
--

INSERT INTO `tb_lingang` (`id`, `nik`, `judul`, `deskripsi`, `tgl_awal`, `tgl_akhir`) VALUES
('LIG-18012100001', '5102223445555221', 'Ngayah', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. \r\nMolestias maxime perferendis voluptates fuga. \r\nAutem aliquam sit iusto sed, necessitatibus quas ipsam adipisci ex qui iste, odit, dolore officia, eius ab?', '2018-01-22', '2018-01-24');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `nik` char(16) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tmp_lahir` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `agama` varchar(20) NOT NULL,
  `jk` enum('laki-laki','perempuan') NOT NULL,
  `alamat` text NOT NULL,
  `status` enum('kawin','belum kawin') NOT NULL,
  `nokk` char(16) NOT NULL,
  `password` varchar(255) NOT NULL,
  `hak_akses` tinyint(4) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`nik`, `nama`, `tmp_lahir`, `tgl_lahir`, `agama`, `jk`, `alamat`, `status`, `nokk`, `password`, `hak_akses`, `photo`, `token`) VALUES
('5102223445555221', 'Agus Wahyu', 'Denpasar', '1998-06-09', 'Hindu', 'laki-laki', 'Dalung Permai Blok NN 10', 'belum kawin', '5102223445665221', '4798147157bab95149dbf45eae2413b7', 3, NULL, ''),
('5102223445555222', 'Saputra', 'Badung', '1994-02-09', 'Islam', 'laki-laki', 'Jakarta Selatan', 'kawin', '5102223445665221', '5350e4ecdf6983acfe8ae7ca197593e9', 2, 'uploads/img/c64c3c6c8c9616cdf164c067d4d7321b.jpg', ''),
('5102223445555223', 'Luh Putu', 'Denpasar', '1992-01-10', 'Budha', 'perempuan', 'Sekitaran Jimbaran', 'belum kawin', '5102223445665221', 'f4edca89ed2030958e1a69bd7e656f75', 3, NULL, ''),
('5102223445555224', 'Putri Ayu', 'Karangasem', '1968-06-11', 'Hindu', 'perempuan', 'Daerah terpencing pokoknya', 'kawin', '5102223441155221', '4bf7d27c35fd877caa1c3d2f43d56ee9', 3, NULL, NULL),
('5102223445555225', 'Bima', 'Karangasem', '1968-10-12', 'Islam', 'laki-laki', 'Tersesat di dunia maya', 'kawin', '5102223445555221', '8e6c4f5de786b1be92144e7d1f9fc32c', 3, NULL, NULL),
('5102223445555226', 'Arjuna', 'Tabanan', '1988-03-13', 'Hindu', 'laki-laki', 'Sedang mencari cinta', 'belum kawin', '5102223441155221', '2290a3677c0265a21a6521b877507d61', 3, NULL, NULL),
('5102223445555227', 'Yudistira', 'Klungkung', '1998-06-14', 'Hindu', 'laki-laki', 'Diam-diam saja', 'belum kawin', '5102223445225221', 'fd48cd19b4c6f46e9c4cbe2deaa88f4b', 3, NULL, ''),
('5102223445555228', 'Fitri', 'Denpasar', '1978-06-15', 'Kristen', 'perempuan', 'Ada deh pokoknya', 'belum kawin', '5102223443355221', 'd3cb91928506a08f06d24842fb9c70a4', 3, NULL, NULL),
('5102223445555229', 'Dewi', 'Klungkung', '1988-02-16', 'Hindu', 'perempuan', 'Mau tau aja', 'belum kawin', '5102223445555221', '6d43e0e5831522904a28c234b55dfb5b', 3, NULL, NULL);

--
-- Triggers `tb_user`
--
DELIMITER $$
CREATE TRIGGER `delete_user` BEFORE DELETE ON `tb_user` FOR EACH ROW BEGIN
  DELETE FROM tb_lapor WHERE nik = OLD.nik;
    DELETE FROM tb_gatra WHERE nik = OLD.nik;
    DELETE FROM tb_lingang WHERE nik = OLD.nik;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `tb_banjar`
--
ALTER TABLE `tb_banjar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_cat_gatra`
--
ALTER TABLE `tb_cat_gatra`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_cat_lapor`
--
ALTER TABLE `tb_cat_lapor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_gatra`
--
ALTER TABLE `tb_gatra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cat_gatra` (`id_cat_gatra`),
  ADD KEY `nik` (`nik`);

--
-- Indexes for table `tb_kaka`
--
ALTER TABLE `tb_kaka`
  ADD PRIMARY KEY (`nokk`),
  ADD KEY `tb_kaka_ibfk_2` (`id_kepala`),
  ADD KEY `id_banjar` (`id_banjar`);

--
-- Indexes for table `tb_lapor`
--
ALTER TABLE `tb_lapor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nik` (`nik`),
  ADD KEY `id_cat_lapor` (`id_cat_lapor`);

--
-- Indexes for table `tb_lingang`
--
ALTER TABLE `tb_lingang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nik` (`nik`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`nik`),
  ADD KEY `nokk` (`nokk`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_gatra`
--
ALTER TABLE `tb_gatra`
  ADD CONSTRAINT `tb_gatra_ibfk_1` FOREIGN KEY (`id_cat_gatra`) REFERENCES `tb_cat_gatra` (`id`),
  ADD CONSTRAINT `tb_gatra_ibfk_2` FOREIGN KEY (`nik`) REFERENCES `tb_user` (`nik`);

--
-- Constraints for table `tb_kaka`
--
ALTER TABLE `tb_kaka`
  ADD CONSTRAINT `tb_kaka_ibfk_1` FOREIGN KEY (`id_banjar`) REFERENCES `tb_banjar` (`id`),
  ADD CONSTRAINT `tb_kaka_ibfk_2` FOREIGN KEY (`id_banjar`) REFERENCES `tb_banjar` (`id`);

--
-- Constraints for table `tb_lapor`
--
ALTER TABLE `tb_lapor`
  ADD CONSTRAINT `tb_lapor_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `tb_user` (`nik`),
  ADD CONSTRAINT `tb_lapor_ibfk_2` FOREIGN KEY (`id_cat_lapor`) REFERENCES `tb_cat_lapor` (`id`);

--
-- Constraints for table `tb_lingang`
--
ALTER TABLE `tb_lingang`
  ADD CONSTRAINT `tb_lingang_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `tb_user` (`nik`),
  ADD CONSTRAINT `tb_lingang_ibfk_2` FOREIGN KEY (`nik`) REFERENCES `tb_user` (`nik`);

--
-- Constraints for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD CONSTRAINT `tb_user_ibfk_3` FOREIGN KEY (`nokk`) REFERENCES `tb_kaka` (`nokk`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
