<?php $this->load->view('__layouts/adminheader') ?>
<section id="content">
    <div class="container-fluid">
        <div class="top-content clearfix">
            <h3>e-Banjar</h3>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url('control-panel') ?>">Dashboard</a>
                </li>
                <li>
                    e-Banjar
                </li>
            </ol>
        </div>
        <div class="button-action">
			<a href="<?php echo base_url('control-panel/banjar/insert') ?>" class="btn btn-success"><i class="fa fa-plus"></i> Banjar</a>
		</div>

		<?php echo $this->session->flashdata('log') ?>
        <div class="panel">
			<div class="panel-body">
				<h4>List Banjar</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>No</th>
								<th>ID</th>
								<th>Nama</th>
								<th>Jumlah Penduduk</th>
								<th>Alamat</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($banjar) > 0) {?>
								<?php $no=1; foreach ($banjar as $value) { ?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td><?php echo $value->id ?></td>
										<td><?php echo $value->nama ?></td>
										<td><label class="label label-default"><?php echo $value->jml ?></label></td>
										<td><?php echo $value->alamat ?></td>
										<td>
											<a href="<?php echo base_url('control-panel/kaka/'.$value->id) ?>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Penduduk</a></a> 
											<a href="<?php echo base_url('control-panel/banjar/'.$value->id.'/edit') ?>" class="btn btn-warning btn-xs"><i class="fa fa-check-square-o"></i> Banjar</a> 
											<?php echo form_open('control-panel/banjar/'.$value->id.'/delete',  'style="display: inline-block;"') ?>
												<button onclick="return confirm('Menjalankan aksi ini akan mengakibatkan terhapusnya data penduduk di banjar tersebut. Yakin ingin melanjutkan aksi?')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Banjar</button>
											<?php echo form_close() ?>
										</td>
									</tr>
								<?php } ?>
							<?php }else{ ?>
								<tr>
									<td colspan="6">No data</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </div>
</section>
<?php $this->load->view('__layouts/adminfooter') ?>