<?php $this->load->view('__layouts/adminheader') ?>
<section id="content">
	<div class="container-fluid">
		<div class="top-content clearfix">
			<h3>e-Gatra <small>Tambah</small></h3>
			<ol class="breadcrumb">
				<li>
					<a href="<?php echo base_url('control-panel') ?>">Dashboard</a>
				</li>
				<li>
					<a href="<?php echo base_url('control-panel/banjar') ?>">e-Banjar</a>
				</li>
				<li>
					Tambah
				</li>
			</ol>
		</div>

		<?php echo $this->session->flashdata('log') ?>
		<div class="row">
			<div class="col-md-9">
				<div class="panel">
					<div class="panel-body">
						<?php echo form_open_multipart('control-panel/banjar/insert') ?>
							<div class="form-group">
								<label>Nama</label>
								<input type="text" class="form-control" name="nama" value="<?php echo set_value('nama'); ?>">
								<?php echo form_error('nama', '<span class="text-error">', '</span>'); ?>
							</div>
							<div class="form-group">
								<label>Deskripsi</label>
								<textarea name="deskripsi" class="form-control" rows="5"><?php echo set_value('deskripsi'); ?></textarea>
								<?php echo form_error('deskripsi', '<span class="text-error">', '</span>'); ?>
							</div>
							<div class="form-group">
								<label>Alamat</label>
								<textarea name="alamat" class="form-control" rows="3"><?php echo set_value('alamat'); ?></textarea>
								<?php echo form_error('alamat', '<span class="text-error">', '</span>'); ?>
							</div>
							<div class="form-group">
								<label>Gambar <small>(maxsize: 500kb|filetype: png,jpg,jpeg)</small></label>
								<input type="file" name="gambar">
							</div>
					</div><!-- end panel -->
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel">
					<div class="panel-body">
							<button class="btn btn-primary btn-block" onclick="return confirm('Yakin ingin melanjukan aksi?')"><i class="fa fa-save"></i> Simpan</button>
						<?php echo form_close() ?>
					</div><!-- end panel -->
				</div>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('__layouts/adminfooter') ?>
