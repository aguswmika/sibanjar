<?php $this->load->view('__layouts/adminheader') ?>
<style>
    .item img{
        width: 100%;
        height: 100%;
    }
    .newsh-title{
        margin-left: 20px;
        color: white;
        position: absolute;
        bottom: 0;
    }
    .news-row img{
        width: 100%;
        height: auto;
    }
    .berita-utama{
        position: absolute;
        top: 125px;
        left: 10px;
        z-index: 10;
        margin-left: 10px;
        background-color: #ffffff;
        padding-right: 10px;
        padding-left: 10px;
    }
    .berita-utama h3{
        margin-top: 10px;
    }
    .berita-terkini{
        border-bottom: 1px solid #dfdfdf;
        padding-bottom: 10px;
        margin-bottom: 10px;
        height: 20%;
    }
    .berita-terkini img{
        width: 100%;
    }
    .judul-terkini {
        font-size: 24px;
    }
    .tanggal-terkini {
    }
    .berita-populer{
        margin-top: 10px;
        padding-top: 10px;
        border-top: 1px solid #dfdfdf;
    }
    .berita-populer .row{
        margin-top: 10px;
    }
    .berita-pilihan{
        margin-top: 10px;
        padding-top: 10px;
        border-top: 1px solid #dfdfdf;
    }
    .berita-pilihan .row{
        margin-top: 10px;
    }
    .kategori-berita{
        width: auto;
        height: auto;
        list-style-type: none;
        float: left;
    }
    .kategori-berita ul li{
        display: inline-block;
        margin-right: 5px;
        padding-right: 10px;
        border-right: 1px solid #dfdfdf;
        font-size: 20px;
    }
    .search{
        float: right;
        border: none;
        width: 30%;
    }
    .no-populer{
        /*margin-top: -15px;*/
        font-size: 50px;
        margin: 0px 0px 0px 0px;
    }
    .no-populer{
    }
    .border-populer{
        float: right;
        width: 80%;
        border-top: 1px solid #dfdfdf;
    }
    .judul-populer{
        font-size: 100%;
    }
</style>
<section id="content">
    <div class="container-fluid">
        <div class="top-content clearfix">
            <h3>e-Gatra</h3>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url('control-panel') ?>">Dashboard</a>
                </li>
                <li>
                    <a href="#!">e-Gatra</a>
                </li>
            </ol>
        </div>
		<div class="row">
            <div class="col-md-9">
                <div class="panel nav-gatra">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-8 col-sm-12 col-xs-12 kategori-berita">
                                <ul style="padding: 0px; margin-bottom: 0px;">
                                    <li>Olah Raga</li>
                                    <li>Politik</li>
                                    <li>Hukum</li>
                                    <li>Budaya</li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 search">
                                <input class="form-control" type="search" name="search" placeholder="Search...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <div class="berita-utama">
                            <h3>BERITA UTAMA</h3>
                        </div>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                <li data-target="#myCarousel" data-slide-to="3"></li>
                            </ol>
                  
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">

                                    <img src="<?php echo base_url("assets/") ?>img/pohon.jpg"><h2 class="newsh-title">Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h2>
                                </div>
                  
                                <div class="item">
                                    <a href="indexpanel.php?page=e_gatra_news"><img src="<?php echo base_url("assets/") ?>img/gunungagung.jpg"><h2 class="newsh-title">Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h2></a>
                                </div>
                                <div class="item">

                                    <img src="<?php echo base_url("assets/") ?>img/pohon.jpg"><h2 class="newsh-title">Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h2>
                                </div>
                  
                                <div class="item">
                                    <a href="indexpanel.php?page=e_gatra_news"><img src="<?php echo base_url("assets/") ?>img/gunungagung.jpg"><h2 class="newsh-title">Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h2></a>
                                </div>
                            </div>
                  
                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                        <div>
                            <h3>BERITA TERKINI</h3>
                        </div>
                        <div class="berita-terkini">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="<?php echo base_url("assets/") ?>img/kerja.jpg">
                                </div>
                                <div class="col-md-9">
                                    <p class="judul-terkini">Seluruh Warga Banjar Kerja Bakti Membersihkan Lingkungan</p>
                                    <p class="tanggal-terkini">21/01/2018, 14:36 WIB</p>
                                </div>
                            </div>
                        </div>
                        <div class="berita-terkini">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="<?php echo base_url("assets/") ?>img/kerja.jpg">
                                </div>
                                <div class="col-md-9">
                                    <p class="judul-terkini">Seluruh Warga Banjar Kerja Bakti Membersihkan Lingkungan</p>
                                    <p class="tanggal-terkini">21/01/2018, 14:36 WIB</p>
                                </div>
                            </div>
                        </div>
                        <div class="berita-terkini">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="<?php echo base_url("assets/") ?>img/kerja.jpg">
                                </div>
                                <div class="col-md-9">
                                    <p class="judul-terkini">Seluruh Warga Banjar Kerja Bakti Membersihkan Lingkungan</p>
                                    <p class="tanggal-terkini">21/01/2018, 14:36 WIB</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h3>BERITA LAINNYA</h3>
                        </div>
                        <div class="row news-row">
                            <div class="col-md-4 zero-padding">
                                <img src="<?php echo base_url("assets/") ?>img/kerja.jpg">
                                <div class="back-title"><h5>Seluruh Warga Banjar Kerja Bakti Membersihkan Lingkungan</h5></div>
                            </div>
                            <div class="col-md-4 zero-padding">
                                <img src="<?php echo base_url("assets/") ?>img/pohon2.jpg">
                                <div class="back-title"><h5>Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h5></div>
                            </div>
                            <div class="col-md-4 zero-padding">
                                <img src="<?php echo base_url("assets/") ?>img/pohon2.jpg">
                                <div class="back-title"><h5>Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h5></div>
                            </div>
                        </div>
                    </div>
                </div><!-- end panel -->
                
            </div>
            <div class="col-md-3">
                <div class="panel">
                    <div class="panel-body">
                        <h3 style="margin-top: 0px;">TERPOPULER</h3>
                        <div class="berita-populer">
                            <div class="row">
                                <div class="col-md-2" >
                                    <p class="no-populer">1</p>
                                </div>
                                <div class="col-md-9">
                                    <p class="judul-populer">Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</p>
                                </div>
                            </div>
                            <div class="border-populer"></div>
                            <div class="row">
                                <div class="col-md-2" >
                                    <p class="no-populer">2</p>
                                </div>
                                <div class="col-md-9">
                                    <p class="judul-populer">Seluruh Warga Banjar Kerja Bakti Membersihkan Lingkungan</p>
                                </div>
                            </div>
                            <div class="border-populer"></div>
                            <div class="row">
                                <div class="col-md-2" >
                                    <p class="no-populer">3</p>
                                </div>
                                <div class="col-md-9">
                                    <p class="judul-populer">Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <h3 style="margin-top: 0px;">BERITA PILIHAN</h3>
                        <div class="berita-pilihan">
                            <div class="row">
                                <div class="col-md-2" >
                                    <p class="no-populer">1</p>
                                </div>
                                <div class="col-md-9">
                                    <p class="judul-populer">Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</p>
                                </div>
                            </div>
                            <div class="border-populer"></div>
                            <div class="row">
                                <div class="col-md-2" >
                                    <p class="no-populer">2</p>
                                </div>
                                <div class="col-md-9">
                                    <p class="judul-populer">Seluruh Warga Banjar Kerja Bakti Membersihkan Lingkungan</p>
                                </div>
                            </div>
                            <div class="border-populer"></div>
                            <div class="row">
                                <div class="col-md-2" >
                                    <p class="no-populer">3</p>
                                </div>
                                <div class="col-md-9">
                                    <p class="judul-populer">Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

	</div>
</section>
<?php $this->load->view('__layouts/adminfooter') ?>
