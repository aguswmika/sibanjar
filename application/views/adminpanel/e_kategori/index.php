<?php $this->load->view('__layouts/adminheader') ?>
<section id="content">
    <div class="container-fluid">
        <div class="top-content clearfix">
            <h3>e-Kategori</h3>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url('control-panel') ?>">Dashboard</a>
                </li>
                <li>
                    e-Kategori
                </li>
            </ol>
        </div>
        <?php echo $this->session->flashdata('log') ?>
        <div class="button-action">
			<a href="<?php echo base_url('control-panel/kategori/gatra/insert') ?>" class="btn btn-success"><i class="fa fa-plus"></i> Gatra</a>
		</div>
        <div class="panel">
			<div class="panel-body">
				<h4>List Kategori Gatra</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>No</th>
								<th>ID</th>
								<th>Judul</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($cat_gatra) > 0) {?>
								<?php $no=1; foreach ($cat_gatra as $value) { ?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td><?php echo $value->id ?></td>
										<td><?php echo $value->judul ?></td>
										<td>
											<a href="<?php echo base_url('control-panel/kategori/gatra/'.$value->id.'/edit') ?>" class="btn btn-warning btn-xs"><i class="fa fa-check-square-o"></i> Gatra</a> 
											<?php echo form_open('control-panel/kategori/gatra/'.$value->id.'/delete',  'style="display: inline-block;"') ?>
												<button onclick="return confirm('Menjalankan aksi ini akan mengakibatkan terhapusnya data penduduk di banjar tersebut. Yakin ingin melanjutkan aksi?')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Gatra</button>
											<?php echo form_close() ?>
										</td>
									</tr>
								<?php } ?>
							<?php }else{ ?>
								<tr>
									<td colspan="4">No data</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="button-action">
			<a href="<?php echo base_url('control-panel/kategori/lapor/insert') ?>" class="btn btn-success"><i class="fa fa-plus"></i> Lapor</a>
		</div>
		<div class="panel">
			<div class="panel-body">
				<h4>List Kategori Lapor</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>No</th>
								<th>ID</th>
								<th>Judul</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($cat_lapor) > 0) {?>
								<?php $no=1; foreach ($cat_lapor as $value) { ?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td><?php echo $value->id ?></td>
										<td><?php echo $value->judul ?></td>
										<td>
											<a href="<?php echo base_url('control-panel/kategori/lapor/'.$value->id.'/edit') ?>" class="btn btn-warning btn-xs"><i class="fa fa-check-square-o"></i> Lapor</a> 
											<?php echo form_open('control-panel/kategori/lapor/'.$value->id.'/delete',  'style="display: inline-block;"') ?>
												<button onclick="return confirm('Menjalankan aksi ini akan mengakibatkan terhapusnya data penduduk di banjar tersebut. Yakin ingin melanjutkan aksi?')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Lapor</button>
											<?php echo form_close() ?>
										</td>
									</tr>
								<?php } ?>
							<?php }else{ ?>
								<tr>
									<td colspan="4">No data</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </div>
</section>
<?php $this->load->view('__layouts/adminfooter') ?>