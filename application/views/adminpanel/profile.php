<?php $this->load->view('__layouts/adminheader') ?>

<section id="content">
    <div class="container-fluid">
        <div class="top-content clearfix">
            <h3>Profile</h3>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Dashboard</a>
                </li>
                <li>
                  Profile
                </li>
            </ol>
        </div>
        <div class="row">
        	<div class="col-md-9">
		        <div class="panel">
		            <div class="panel-body">
						<?php echo form_open_multipart('control-panel/profile/'.$this->uri->segment(3)) ?>
			                <div class="form-group">
			                	<label>Password</label>
			                	<input class="form-control" type="password" name="pass">
			                	<?php echo form_error('pass', '<span class="text-error">', '</span>'); ?>
			                </div>
			                <div class="form-group">
			                	<label>Re-password</label>
			                	<input class="form-control" type="password" name="repass">
			                	<?php echo form_error('repass', '<span class="text-error">', '</span>'); ?>
			                </div>
			                <div class="form-group">
			                	
								<label>Foto Profile <small>(maxsize: 500kb|filetype: png,jpg)</small></label><br>
								<?php if(!empty($data->photo)){ ?>
		                            <img style="max-width: 50px;    margin-bottom: 10px;" alt="Foto Profil" src="<?php echo base_url($data->photo)?>">
		                        <?php } ?>
								<input type="file" name="gambar">
							</div>
		            </div>
		        </div>
	        </div>
	        <div class="col-md-3">
				<div class="panel">
					<div class="panel-body">
							<button class="btn btn-primary btn-block" onclick="return confirm('Yakin ingin melanjukan aksi?')"><i class="fa fa-save"></i> Simpan</button>
						<?php echo form_close() ?>
					</div><!-- end panel -->
				</div>
			</div>
        </div>
    </div>
</section>