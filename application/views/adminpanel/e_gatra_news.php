
<?php $this->load->view('__layouts/adminheader') ?>

<div class="container">
	<div class="row">
		<div class="col-md-9 news-container">
			<div class="row title-box">
				<div class="news-title col-md-12"><h1>Warga Terpaksa Mengungsi Akibat Erupsi Gunung Agung</h1></div>
				<div class="time col-md-12"><small>Jumat, 1 Desember 2017</small></div>
			</div>
			<div class="row sharing-box">
				<div class="sharing-btn">

					<img src="../img/facebook.png">
				</div>
				<div class="sharing-btn">
					<img src="../img/twitter.png">
				</div>
				<div class="sharing-btn">
					<img src="../img/google.png">

					<img src="<?php echo base_url("assets/") ?>img/facebook.png">
				</div>
				<div class="sharing-btn">
					<img src="<?php echo base_url("assets/") ?>img/twitter.png">
				</div>
				<div class="sharing-btn">
					<img src="<?php echo base_url("assets/") ?>img/google.png">

				</div>
				<div class="sharing-btn">
					<img src="">
				</div>
			</div>
			<div class="row foto-box">

				<img src="../img/gunungagung.jpg">

				<img src="<?php echo base_url("assets/") ?>img/gunungagung.jpg">

			</div>
			<div class="row content-box text-justify">
				<p><b>Arus</b> pengungsi dari wilayah terdampak erupsi Gunung Agung di Provinsi Bali ke berbagai lokasi yang dianggap aman hingga siang ini terus meningkat. Hingga siang ini, Senin (25/9) sebanyak 59.820 jiwa warga terpaksa meninggalkan rumah mereka di tengah kekhawatiran erupsi dalam waktu dekat."Hingga saat ini hampir 60 ribu jiwa yang mengungsi," kata Dirjen Perlindungan dan Jaminan Sosial Kementerian Sosial Harry Hikmat
				yang dihubungi dari Jakarta, Senin (25/9). Jumlah ini naik sekitar 30 persen, dari waktu tengah malam, Minggu (24/9) yang tercatat sekitar 42 ribu warga mengungsi ke tempat-tempat yang aman.</p>
				<p>Harry menjelaskan, warga yang mengungsi tersebar di sejumlah titik, antara lain, di Kabupaten Klungkung jumlah pengungsi sebanyak 14.808 jiwa yang tersebar di 114 titik pengungsian dengan titik pengungsian terbesar di GOR Sueca Pura Gel Gel sebanyak 3.789 jiwa. Di Kabupaten Karangasem jumlah pengungsi sebanyak 30.012 jiwa yang tersebar di enam kecamatan. Di Kabupaten Bangli, warga yang mengungsi mencapai 7.047 jiwa yang tersebar di 23 titik pengungsian.</p>
				<p>Selain itu, sebanyak 6.486 jiwa warga Kabupaten Buleleng mengungsi ke 11 titik pengungsian yang tersebar di daerah itu serta Kota Denpasar tercatat pengungsi sebanyak 1.467 jiwa di 15 titik. "Sementara itu dari Kabupaten Tabanan dan Badung sedang dalam pendataan," tambah Harry Hikmat.</p>
				<p>Dia juga menyebutkan upaya-upaya yang dilakukan Kementerian Sosial dalam penanganan siaga darurat erupsi Gunung Agung antara lain, melakukan koordinasi dengan Badan Nasional Penanggulangan Bencana (BNPB), Pemprov Bali serta dinsos setempat.</p>
				<p>Selain itu, Kemsos juga mengerahkan personel baik Taruna Siaga Bencana dan pilar sosial lainnya serta pengerahan peralatan evakuasi, mobilitas dan sarana pendukung lainnya. Adapun, Dinsos Karangasem telah mengeluarkan Cadangan Beras Pemerintah (CBP) sebanyak 62 ton untuk kebutuhan pengungsi di Karangasem, Dinsos Provinsi Bali menyiagakan dan mendistribusikan beras reguler sebanyak 50 ton untuk distribusi Kabupaten Bangli, Buleleng dan Klungkung masing-masing 10 ton.</p>
				<p>Kemsos juga mendirikan dapur umum lapangan di 10 titik yaitu di Posko Candi Kuning Bedugul Tabanan, Posko Dinas Sosial Bangli, Posko Dinsos Kota Denpasar, Posko Klungkung 2, Posko Klungkung 3, Posko GOR Sueca Pura Gelgel, Posko Bandem Karangasem, Posko Ulakan, Posko Utama Karangasem dan Posko Tembok Buleleng.</p>
			</div>
			<div class="row sharing-box" style="margin-top:50px">
				<div class="sharing-btn">

					<img src="../img/facebook.png">
				</div>
				<div class="sharing-btn">
					<img src="../img/twitter.png">
				</div>
				<div class="sharing-btn">
					<img src="../img/google.png">

					<img src="<?php echo base_url("assets/") ?>img/facebook.png">
				</div>
				<div class="sharing-btn">
					<img src="<?php echo base_url("assets/") ?>img/twitter.png">
				</div>
				<div class="sharing-btn">
					<img src="<?php echo base_url("assets/") ?>img/google.png">

				</div>
				<div class="sharing-btn">
					<img src="">
				</div>
			</div>
			<div class="row berita-terkait">
				<h4>Berita Terkait</h4>
				<ul>
					<li>Erupsi Gunung Agung, Lion Air Siap Refund Penuh Tiket Penerbangan</li>
					<li>Ini Alasan Bandara Ngurah Rai Kembali Dibuka</li>
					<li>Siaga Darurat Gunung Agung,Pemerintah Siapkan Dana Rp 7,5 Milyar</li>
					<li>Cerita Terkait Letusan Gunung Agung dan Sihir di Bali</li>
				</ul>
			</div>
			<div class="form-group" style="margin-top:50px;margin-left:85px;">
				<label for="Comment">Komentar</label>
				<textarea class="form-control form-comment" rows="5" id="comment"></textarea>
				<button type="submit" class="btn button-send">Kirimkan</button>
			</div>
		</div>
		<div class="col-md-3">
				<div class="col-md-12">
				    <div class="input-group" style="margin-top:25px ">
				     	<input type="search" class="form-control" placeholder="cari" style="border-top-left-radius: 20px;border-bottom-left-radius: 20px;">
				      	<span class="input-group-btn">
				        	<button class="btn tombol" type="button"><i class="fa fa-search"></i></button>
				      	</span>
				    </div>
				</div>
				<div class="col-md-12" style="margin-top:25px;">
					<div class="news-popular">
						<div class="pnews-box">
							<div class="pnews-title" style="text-align: center;">
								Berita Terpopuler
							</div>
						</div>
						<div class="pnews-box">
							<div class="col-md-2 pnews-number"style="background-color:#ff9775;">
								1
							</div>
							<div class="col-md-10 pnews-title">
								Gunung Agung
							</div>
						</div>
						<div class="pnews-box">
							<div class="col-md-2 pnews-number" style="background-color:#ff855d;">
								2
							</div>
							<div class="col-md-10 pnews-title">
								Banjir Melanda
							</div>
						</div>
						<div class="pnews-box">
							<div class="col-md-2 pnews-number"style="background-color:#f0774f;">
								3
							</div>
							<div class="col-md-10 pnews-title">
								Pohon Tumbang
							</div>
						</div>
						<div class="pnews-box">
							<div class="col-md-2 pnews-number"style="background-color:#d86e4a;">
								4
							</div>
							<div class="col-md-10 pnews-title">
								Kerja Bakti Bersama
							</div>
						</div>
						<div class="pnews-box">
							<div class="col-md-2 pnews-number"style="background-color:#b5593b;">
								5
							</div>
							<div class="col-md-10 pnews-title">
								Lomba 17 Agustus 2017
							</div>
						</div>
					</div>
					<div class="news-popular anews">
						<div class="pnews-box">
							<div class="pnews-title" style="text-align: center;">
								Artikel Terpopuler
							</div>
						</div>
						<div class="pnews-box">
							<div class="col-md-2 pnews-number"style="background-color:#ff9775;">
								1
							</div>
							<div class="col-md-10 pnews-title">
								Jenis Jenis Banten
							</div>
						</div>
						<div class="pnews-box">
							<div class="col-md-2 pnews-number"style="background-color:#ff855d;">
								2
							</div>
							<div class="col-md-10 pnews-title">
								Jenis Jenis Kulkul
							</div>
						</div>
						<div class="pnews-box">
							<div class="col-md-2 pnews-number"style="background-color:#f0774f;">
								3
							</div>
							<div class="col-md-10 pnews-title">
								Tips Tips Unik
							</div>
						</div>
						<div class="pnews-box">
							<div class="col-md-2 pnews-number"style="background-color:#d86e4a;">
								4
							</div>
							<div class="col-md-10 pnews-title">
								Cara Membuat Ketupat
							</div>
						</div>
						<div class="pnews-box">
							<div class="col-md-2 pnews-number"style="background-color:#b5593b;">
								5
							</div>
							<div class="col-md-10 pnews-title">
								Kumpulan Doa
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>

</div>

</div>

<?php $this->load->view('__layouts/adminfooter') ?>
