<div class="container">
	<div class="row">
        <div class="row">
            <center><h1 style="margin-top: 40px;">Laporkan Masalah di Lingkungan Banjar</h1></center>
            <div class="col-md-offset-3 col-md-6">
                <div class="lapor-box">
                    <form action="" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Nama Masalah</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Kategori Masalah</label>
                            <select name="" id="" class="form-control"> 
                                <option value="">-- Pilih kategori --</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="" id="" rows="5" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Foto</label>
                            <input type="file" multiple>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger btn-block"><i class="fa fa-bullhorn"></i> Laporkan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>