<?php $this->load->view('__layouts/adminheader') ?>
<section id="content">
	<div class="container-fluid">
		<div class="top-content clearfix">
			<h3>e-Lingang <small>Tambah</h3>
			<ol class="breadcrumb">
				<li>
					<a href="<?php echo base_url('control-panel') ?>">Dashboard</a>
				</li>
				<li>
					<a href="<?php echo base_url('control-panel/lingang') ?>">e-Lingang</a>
				</li>
				<li>
					Tambah
				</li>
			</ol>
		</div>

		<?php echo $this->session->flashdata('log') ?>
		<div class="row">
			<div class="col-md-9">
				<div class="panel">
					<div class="panel-body">
						<?php echo form_open_multipart('control-panel/lingang/insert') ?>
							<div class="form-group">
								<label>Nama</label>
								<input type="text" class="form-control" name="nama" value="<?php echo set_value('nama'); ?>">
								<?php echo form_error('nama', '<span class="text-error">', '</span>'); ?>
							</div>
							<div class="form-group">
								<label>Deskripsi</label>
								<textarea name="deskripsi" class="form-control" rows="5"><?php echo set_value('deskripsi'); ?></textarea>
								<?php echo form_error('deskripsi', '<span class="text-error">', '</span>'); ?>
							</div>
							<div class="form-group">
								<label>Tanggal Mulai</label>
								<input type="date" class="form-control" name="start" value="<?php echo set_value('start'); ?>">
								<?php echo form_error('start', '<span class="text-error">', '</span>'); ?>
							</div>
							<div class="form-group">
								<label>Tanggal Berakhir</label>
								<input type="date" class="form-control" name="end" value="<?php echo set_value('end'); ?>">
								<?php echo form_error('end', '<span class="text-error">', '</span>'); ?>
							</div>
					</div><!-- end panel -->
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel">
					<div class="panel-body">
							<button class="btn btn-primary btn-block" onclick="return confirm('Yakin ingin melanjukan aksi?')"><i class="fa fa-save"></i> Simpan</button>
						<?php echo form_close() ?>
					</div><!-- end panel -->
				</div>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('__layouts/adminfooter') ?>
