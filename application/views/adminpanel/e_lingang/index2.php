<?php $this->load->view('__layouts/adminheader') ?>
<link rel="stylesheet" href="<?php echo base_url('assets/') ?>scripts/fullcalendar/fullcalendar.min.css" />
<style>
	.fc-day-number{
		font-size: 30px;
	}
	.fc-sun{
		color: red;
	}
	.fc-unthemed td.fc-today{
		background-color: #b5593b;
		color: white;
	}
	.fc-time{
		display: none;
	}
	.fc-event{
		text-align: center;
		padding: 5px;
		background-color: #5cb85c;
		border-color: #5cb85c;
	}
	.lingang-content label{
		color: #757575;
	}
	.lingang-content span{
		font-size: 18px;
		display: block;
	}
</style>
<section id="content">
	<div class="container-fluid">
		<div class="top-content clearfix">
			<h3>e-Lingang</h3>
			<ol class="breadcrumb">
				<li>
					<a href="<?php echo base_url('control-panel') ?>">Dashboard</a>
				</li>
				<li>
					<a href="#!">e-Lingang</a>
				</li>
			</ol>
		</div>
		<div class="panel">
			<div class="panel-body">
				<div id="calendar" class="max-width-calendar"></div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="showAcara" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Lihat Acara</h4>
			</div>
			<div class="modal-body">
				<div class="lingang-content">
					<label>Nama: </label>
					<span id="nama"></span>
				</div>
				<div class="lingang-content">
					<label>Keterangan: </label>
					<span id="deskripsi"></span>
				</div>
				<div class="lingang-content">
					<label>Tanggal: </label>
					<span id="tanggal"></span>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>

</div>

<?php $this->load->view('__layouts/adminfooter') ?>
<script src="<?php echo base_url('assets/') ?>scripts/fullcalendar/lib/moment.min.js"></script>
<script src="<?php echo base_url('assets/') ?>scripts/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url('assets/') ?>scripts/fullcalendar/gcal.js"></script>
<script src='<?php echo base_url('assets/') ?>scripts/fullcalendar/locale-all.js'></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#calendar').fullCalendar({
			locale:'id'
		});
	});
	$('#calendar').fullCalendar({
		events: '<?php echo base_url('control-panel/lingang/event') ?>',
		eventDataTransform: function (rawEventData) {
			return {
				title: rawEventData.title,
				start: rawEventData.start,
				end: rawEventData.end,
				id	: rawEventData.id
			};
		},
		eventClick: function(calEvent, jsEvent, view) {
			$.ajax({
				url  	 : '<?php echo base_url('control-panel/lingang/event') ?>',
				type 	 : 'GET',
				dataType : 'JSON',
				data 	 : {id: calEvent.id},
				success  : function(data){
					var desc = data.deskripsi.replace(/\n|\r\n|\r/g, "<br>")
					$('#nama').text(data.title);
					$('#deskripsi').html(desc);
					$('#tanggal').text(data.start+' - '+data.end);
					$('#showAcara').modal('show');

				}
			});
		}
	});
</script>