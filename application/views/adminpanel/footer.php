	<script src="../js/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
   	<script src="../scripts/fullcalendar/lib/moment.min.js"></script>
   	<script src="../scripts/fullcalendar/fullcalendar.min.js"></script>
   	<script src="../scripts/fullcalendar/gcal.js"></script>
   	<script src='../scripts/fullcalendar/locale-all.js'></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#calendar').fullCalendar({
				locale:'id'
			});
		});
		$('#calendar').fullCalendar({
		    events: [
		        {
		            title  : 'Banjar Melali',
		            start  : '2017-12-06',
		            end	   : '2017-12-08'
		        },
		        {
		            title  : 'Bazzkom',
		            start  : '2017-12-21',
		            end    : '2017-12-24'
		        }
		    ]
		});
	</script>
</body>
</html>