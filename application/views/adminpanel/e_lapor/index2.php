<?php $this->load->view('__layouts/adminheader') ?>
<style>
	.lapor-content label{
		color: #757575;
	}
	.lapor-content span{
		font-size: 18px;
		display: block;
	}
</style>
<section id="content">
	<div class="container-fluid">
		<div class="top-content clearfix">
			<h3>e-Lapor</h3>
			<ol class="breadcrumb">
				<li>
					<a href="<?php echo base_url('control-panel') ?>">Dashboard</a>
				</li>
				<li>
					e-Lapor
				</li>
			</ol>
		</div>

		<?php echo $this->session->flashdata('log') ?>
	
		<div class="panel">
			<div class="panel-body">
				<h4>List Lapor</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>No</th>
								<th>ID</th>
								<th>Nama</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($lapor) > 0) {?>
								<?php $no=1; foreach ($lapor as $value) { ?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td><?php echo $value->id ?> <button class="btn btn-xs btn-info showing" data-id="<?php echo $value->id ?>"><i class="fa fa-eye"></i></button></td>
										<td><?php echo $value->judul ?></td>
										<td><label class="label label-<?php echo ($value->status == 'ditanggapi') ? 'success' : 'warning'?>"><?php echo ucwords($value->status) ?></label></td>
										<td>
											<?php echo form_open('control-panel/lapor/'.$value->id.'/status', 'style="display: inline-block;"') ?>
												<input type="hidden" name="status" value="<?php echo ($value->status == 'ditanggapi') ? 'belum ditanggapi' : 'ditanggapi' ?>">
												<button class="btn btn-xs btn-<?php echo ($value->status == 'ditanggapi') ? 'warning' : 'success' ?>"><i class="fa fa-cogs"></i> <?php echo ($value->status == 'ditanggapi') ? 'Batal' : 'Tanggapi' ?></button>
											<?php echo form_close() ?>
											<?php echo form_open('control-panel/lapor/'.$value->id.'/delete',  'style="display: inline-block;"') ?>
												<button onclick="return confirm('Yakin ingin melanjutkan aksi?')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Lapor</button>
											<?php echo form_close() ?>
										</td>
									</tr>
								<?php } ?>
							<?php }else{ ?>
								<tr>
									<td colspan="5">No data</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="show" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Lihat Acara</h4>
			</div>
			<div class="modal-body">
				<div class="lapor-content">
					<label>Nama: </label>
					<span id="nama"></span>
				</div>
				<div class="lapor-content">
					<label>Deskripsi: </label>
					<span id="deskripsi"></span>
				</div>
				<div class="lapor-content">
					<label>Kategori: </label>
					<span id="kategori"></span>
				</div>
				<div class="lapor-content">
					<label>Gambar: </label>
					<img id="gambar" style="max-width: 1024px;display: block;">
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('__layouts/adminfooter') ?>
<script>
	$(document).ready(function(){
		$('.showing').click(function(){
			$.ajax({
				url  	 : '<?php echo base_url('control-panel/lapor/show') ?>',
				type 	 : 'GET',
				dataType : 'JSON',
				data 	 : {id: $(this).attr('data-id')},
				success  : function(data){
					console.log(data);
					var desc = data.deskripsi.replace(/\n|\r\n|\r/g, "<br>")
					$('#nama').text(data.judul);
					$('#deskripsi').html(desc);
					$('#kategori').text(data.lp_judul);
					$('#gambar').attr('src', '<?php echo base_url() ?>'+data.gambar);
					$('#show').modal('show');
				}
			});
		})
	})
</script>