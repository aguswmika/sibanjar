<?php $this->load->view('__layouts/adminheader') ?>
<section id="content">
	<div class="container-fluid">
		<div class="top-content clearfix">
			<h3>e-Lapor</h3>
			<ol class="breadcrumb">
				<li>
					<a href="<?php echo base_url('control-panel') ?>">Dashboard</a>
				</li>
				<li>
					e-Lapor
				</li>
			</ol>
		</div>

		<?php echo $this->session->flashdata('log') ?>
		<div class="row">
			<div class="col-md-5">
				<div class="panel">
					<div class="panel-body">
						<h4>Tambah Laporan</h4>
						<?php echo form_open_multipart('control-panel/lapor') ?>
							<div class="form-group">
								<label>Nama</label>
								<input type="text" class="form-control" name="nama" value="<?php echo set_value('nama'); ?>">
								<?php echo form_error('nama', '<span class="text-error">', '</span>'); ?>
							</div>
							<div class="form-group">
								<label>Deskripsi</label>
								<textarea name="deskripsi" class="form-control" rows="5"><?php echo set_value('deskripsi'); ?></textarea>
								<?php echo form_error('deskripsi', '<span class="text-error">', '</span>'); ?>
							</div>
							<div class="form-group">
								<label>Kategori</label>
								<select name="kategori" class="form-control">
									<option value="">-- Pilih --</option>
									<?php foreach ($cat as $value): ?>
										<option value="<?php echo $value->id ?>"><?php echo $value->judul ?></option>
									<?php endforeach ?>
								</select>
								<?php echo form_error('kategori', '<span class="text-error">', '</span>'); ?>
							</div>
							<div class="form-group">
								<label>Gambar (maxsize: 5mB|filetype: png, jpg)</label>
								<input type="file" name="gambar">
							</div>
							<div class="form-group">
								<button class="btn btn-primary btn-block"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="panel">
					<div class="panel-body">
						<h4>List Lapor</h4>
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>No</th>
										<th>ID</th>
										<th>Nama</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php if(count($lapor) > 0) {?>
										<?php $no=1; foreach ($lapor as $value) { ?>
											<tr>
												<td><?php echo $no++ ?></td>
												<td><?php echo $value->id ?></td>
												<td><?php echo $value->judul ?></td>
												<td><label class="label label-<?php echo ($value->status == 'ditanggapi') ? 'success' : 'warning'?>"><?php echo ucwords($value->status) ?></label></td>
											</tr>
										<?php } ?>
									<?php }else{ ?>
										<tr>
											<td colspan="4">No data</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('__layouts/adminfooter') ?>