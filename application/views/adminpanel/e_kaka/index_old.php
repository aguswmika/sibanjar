<?php $this->load->view('__layouts/adminheader') ?>
<style>
    .item img{
        width: 100%;
        height: 100%;
    }
    .newsh-title{
        margin-left: 20px;
        color: white;
        position: absolute;
        bottom: 0;
    }
</style>
<script language="javascript" src="showhidemenu.js"></script>
<section id="content">
    <div class="container-fluid">
        <div class="top-content clearfix">
            <h3>e-Kaka</h3>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo base_url('control-panel') ?>">Dashboard</a>
                </li>
                <li>
                    <a href="#!">e-Kaka</a>
                </li>
            </ol>
        </div>
		
		<div class="panel">
			<div class="panel-body">
	
<div class="container"> 
	<div class="row">
        <div class="row">
                <h4 style="margin-left:40px">
                	Daftar Anggota Banjar Anyar Kaja
                </h4><br />
        	<div class="clearfix" style="float:left; width: 200px; margin-left:40px">
                <form action="" class="clearfix">
                    <div class="input-group">
                        <input type="search" class="form-control" placeholder="Cari" style="border-top-left-radius: 20px;border-bottom-left-radius: 20px;">
                        <span class="input-group-btn">
                        <button class="btn tombol" type="button" style="border-bottom-right-radius:20px;border-top-right-radius:20px;"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
                        <div class="col-md-9">
                           <input type="button" class="btn tombol" style="border-radius: 20px;border-bottom-left-radius: 20px; width:150px;" value="Tambah Keluarga" data-toggle="modal" data-target="#tambahAnggota">
    <div class="modal fade" id="tambahAnggota" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Tambah Keluarga</h4>
			</div>
			<div class="modal-body">
				<form action="" enctype="multipart/form-data">
					<div class="form-group">
						<label>Nomor Kartu Keluarga</label>
						<input type="text" name="nokk" class="form-control">
					</div>
					<div class="form-group">
						<label>Nama Lengkap</label>
						<input type="text" name="nama" class="form-control">
					</div>
					<div class="form-group">
						<label>Alamat</label>
						<input type="text" name="alamat" class="form-control">
					</div>
					<div class="form-group">
						<label>RT</label>
						<input type="text" name="rt" class="form-control">
					</div>
					<div class="form-group">
						<label>Desa</label>
						<input type="text" name="desa" class="form-control">
					</div>
					<div class="form-group">
						<label>Kecamatan</label>
						<input type="text" name="kecamatan" class="form-control">
					</div>
					<div class="form-group">
						<label>Kabupaten</label>
						<input type="text" name="kabupaten" class="form-control">
					</div>
					<div class="form-group">
						<label>Kode Pos</label>
						<input type="text" name="kodepos" class="form-control">
					</div>
					<div class="form-group">
						<label>Provinsi</label>
						<input type="text" name="provinsi" class="form-control">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-close" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>
                        </div>
            <div class="panel" style="margin-top: 50px;">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-9" style="padding:0;">
                        <ul>
                        <li type="none">
                        <h4>Nama Kepala Keluarga
                        <small>| No Kartu Keluarga | Alamat                     
                        </small></h4><a href="?=detail" onclick="myFunction()">Detail...</a>
                        <div id="myDIV">
                        <ul style="padding:0;">
                            <table border="1">
                                <tr>
                                  	<th width="150px">NIK</th>
                                    <th width="200px">Nama</th>
                                    <th width="250px">TTL</th>
                                    <th width="150px">Jenis Kelamin</th>
                                    <th width="100px">Status</th>
                                    <th width="150px" align="center">Aksi</th>
                                </tr>
                                <tr>
                                 	<td>500000000</th>
                                    <td>Teguh</th>
                                    <td>Denpasar, 28 Mei 1999</th>
                                    <td>Laki - laki</th>
                                    <td>Pelajar</th>
                                    <td align="center">Hapus | Edit</th>
                                </tr>
                            </table>
                            </ul>
                            </li>
                            </ul>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                
            </div><!-- end panel -->
		</div>
    </div>
</section>
<?php $this->load->view('__layouts/adminfooter') ?>
