<?php $this->load->view('__layouts/adminheader') ?>
<?php $url = empty(get_user()->id_banjar) ? $this->uri->segment(3) : get_user()->id_banjar; ?>
<style>
	.item img{
		width: 100%;
		height: 100%;
	}
	.newsh-title{
		margin-left: 20px;
		color: white;
		position: absolute;
		bottom: 0;
	}
	.kaka-title{
		font-size: 18px;margin-bottom: 10px;
		float: left;
	}
	.kaka-action{
		float: right;
	}
</style>
<section id="content">
	<div class="container-fluid">
		<div class="top-content clearfix">
			<h3>e-Kaka <?php if(get_user()->hak_akses < 3){ if(empty(get_user()->id_banjar)){ echo "<small>[ ".$this->uri->segment(3)." ]</small>"; }else{  echo "<small>[ ".get_user()->id_banjar." ]</small>"; } }else{ echo ''; }?></h3>
			<ol class="breadcrumb">
				<li>
					<a href="<?php echo base_url('control-panel') ?>">Dashboard</a>
				</li>
				<?php if(empty(get_user()->id_banjar)){ ?>
					<li>
						<a href="<?php echo base_url('control-panel/banjar') ?>">e-Banjar</a>
					</li>
				<?php } ?>
				<li>
					e-Kaka 
				</li>
			</ol>
		</div>

		<div class="button-action">
			<?php if (get_user()->hak_akses < 3): ?>
				<button class="btn btn-success" data-toggle="modal" data-target="#uploadExcel"><i class="fa fa-upload"></i> Penduduk</button>
			<?php endif ?>			
			<?php if(!empty($kelian) && empty(get_user()->id_banjar)) {?>
				<button class="btn btn-info" data-toggle="modal" data-target="#selectKelian"><i class="fa fa-cogs"></i> Kelian</button>
			<?php } ?>
		</div>
		<?php echo $this->session->flashdata('log') ?>
		<?php if(count($kaka) > 0) { ?>
			<?php foreach ($kaka as $item){ ?>
				<div class="clearfix">
					<div class="kaka-title">
						<b>Kepala Keluarga:</b> <?php echo $item['kepala']->nama ?> |
						<b>No.KK:</b> <?php echo $item['kepala']->nokk ?> |
						<b>Alamat:</b> <?php echo $item['kepala']->alamat ?>
					</div>
					<div class="kaka-action">
						<?php $nokk = empty(get_user()->nokk) ? '' : get_user()->nokk ?>
						<?php if($item['kepala']->nokk != $nokk){ ?>
							<?php echo form_open('control-panel/kaka/'.$url.'/delete') ?>
								<input type="hidden" name="id" value="<?php echo $item['kepala']->nokk ?>">
								<button onclick="return confirm('Menjalankan aksi ini akan mengakibatkan terhapusnya data penduduk di setiap fitur. Yakin ingin melanjutkan aksi?')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
							<?php echo form_close() ?>
						<?php } ?>
					</div>
				</div>
				<div class="panel">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th style="width: 20%">NIK</th>
										<th style="width: 20%">Nama</th>
										<th style="width: 20%">TTL</th>
										<th style="width: 10%">Kelamin</th>
										<th style="width: 30%">Alamat</th>
									</tr>
								</thead>
								<tbody>
									 <?php foreach ($item['data'] as $value) {?>
									 	<tr>
									 		<td><?php echo $value->nik ?></td>
									 		<td><?php echo $value->nama ?><?php if($value->hak_akses == 2) {?> <label class="label label-warning label-xs">Kelian</label> <?php } ?></td>
									 		<td><?php echo $value->tmp_lahir ?>, <?php echo $value->tgl_lahir ?></td>
									 		<td><?php echo ucfirst($value->jk) ?></td>
									 		<td><?php echo $value->alamat ?></td>
									 	</tr>
									 <?php } ?>
								</tbody>
							</table>
						</div>
					</div><!-- end panel -->
				</div>
			<?php } ?>
		<?php }else{ ?>
			<p>Belum ada data</p>
		<?php } ?>
	</div>
</section>
<div class="modal fade" id="uploadExcel" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Unggah Data Penduduk</h4>
			</div>
			<div class="modal-body">
				<?php echo form_open_multipart('control-panel/kaka/'.$url.'/excel');?>
				   <div class="form-group">
						<label>File Excel <small>(maxsize: 5mB|filetype: xls, xlsx)</small></label>
					   <input type="file" name="filename">
				   </div>
			</div>
			<div class="modal-footer">
					<button class="btn btn-primary" onclick="return confirm('Yakin ingin melanjutkan?')"><i class="fa fa-upload"></i> Unggah</button>
				<?php echo form_close(); ?>
				<button class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>
<!-- kelian action -->
<?php if(!empty($kelian) && empty(get_user()->id_banjar)) {?>
	<div class="modal fade" id="selectKelian" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Pilih Kelian</h4>
				</div>
				<div class="modal-body">
					<?php echo form_open_multipart('control-panel/kaka/'.$url.'/kelian');?>
					   <div class="form-group">
							<label>Kelian</label>
						   	<input type="search" class="form-control" list="kelian" name="kelian" value="<?php echo $kelian->nik; ?>">
						   	<datalist id="kelian">
						   		<?php foreach ($user as $value) { ?>
						   			<option value="<?php echo $value->nik ?>"><?php echo $value->nik ?> | <?php echo $value->nama ?></option>
						   		<?php } ?>
						   	</datalist>
					   </div>
				</div>
				<div class="modal-footer">
						<button class="btn btn-primary" onclick="return confirm('Yakin ingin melanjutkan?')"><i class="fa fa-upload"></i> Pilih</button>
					<?php echo form_close(); ?>
					<button class="btn btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
<?php $this->load->view('__layouts/adminfooter') ?>
