<?php $this->load->view('__layouts/adminheader') ?>
<style>
    .title-box{

    }
    .title-box h4{
        margin-top: 0px;
        
    }
    .border{
        border-bottom: 1px solid #dfdfdf;
    }
    .isi{
        font-size: 70px;
        text-align: center;
    }
</style>
<section id="content">
    <div class="container-fluid">
        <div class="top-content clearfix">
            <h3>Dashboard</h3>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Dashboard</a>
                </li>
                <li>
                    Home
                </li>
            </ol>
        </div>
    
        <div class="panel">
            <div class="panel-body">
                Welcome to siBanjar

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-body">
                        <div class="title-box">
                            <h4>Jumlah Banjar</h4>
                        </div>
                        <div class="row border"></div>
                        <div class="isi">
                            <p><?php echo $count['banjar'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-body">
                        <div class="title-box">
                            <h4>Jumlah Penduduk</h4>
                        </div>
                        <div class="row border"></div>
                        <div class="isi">
                            <p><?php echo $count['penduduk'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-md-4">
                <div class="panel">
                    <div class="panel-body">
                        <div class="title-box">
                            <h4>Jumlah Laporan</h4>
                        </div>
                        <div class="row border"></div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</section>
<?php $this->load->view('__layouts/adminfooter') ?>