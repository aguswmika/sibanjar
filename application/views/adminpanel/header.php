<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>siBanjar - Panel</title>
	<link rel="shortcut icon" href="../img/logo.png">	
	<link rel="stylesheet" href="../css/font-awesome.min.css">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/pstyle.css">
    <link rel="stylesheet" href="../scripts/fullcalendar/fullcalendar.min.css" />
    <link rel="stylesheet" href="../css/elingang_style.css" />
    <link rel="stylesheet" href="../css/egatra_style.css" />
	<link rel="stylesheet" href="../css/masukkan_berita.css" />
    <link rel="stylesheet" type="text/css" href="../css/egatra_news_style.css">
</head>
<body>
	<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<a class="navbar-brand clearfix" href="#"><img src="../img/Logo.png" alt=""><span>siBanjar</span></a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="headercustom">
				<ul class="nav navbar-nav navbar-right">
					<li id="sign-in"><a href="#!">Logout&nbsp;&nbsp;<i class="fa fa-sign-in"></i></a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>


	    <div class="sidebar-wrapper">

	        <ul class="sidebar-nav">
	            <li class="sidebar-brand">
	            	<a class="btn btn-default btn-lg" href="?page=home">
						<span class="glyphicon glyphicon-home" style="color:#b5593b"></span>
  						<p>Dashboard</p>
					</a>
				</li>
	            <li>
	            	<a class="btn btn-default btn-lg" href="?page=e_gatra">
						<img src="../img/gatra.png" class="icon-sidebar" alt="">
  						<p>e-Gatra</p>
					</a>
	            </li>
	            <li>
	            	<a class="btn btn-default btn-lg" href="?page=e_lingang">
					<img src="../img/kalender.png" class="icon-sidebar" alt="">
  						<p>e-Lingang</p>
					</a>
	            </li>
	            <li>
	            	<a class="btn btn-default btn-lg" href="?page=e_kaka">
						<img src="../img/kaka.png" class="icon-sidebar" alt="">
  						<p>e-Kaka</p>
					</a>
	            </li>
	            <li>
	            	<a class="btn btn-default btn-lg" href="?page=e_lapor">
						<img src="../img/lapor.png" class="icon-sidebar" alt="">
  						<p>e-Lapor</p>
					</a>
	            </li>
	        </ul>
	    </div>
