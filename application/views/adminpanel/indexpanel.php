<?php
	include 'header.php';
?>
    <div class="page-content-wrapper">
        <div class="page-content container-fluid">
                <div class="row" style="background-color: #ecf0f1">
                    <div class="container-fluid">
                        <div class="col-md-12 clearfix">
                            <?php
				                if(isset($_GET['page'])){
                                    if($_GET['page'] == 'home'){
                                        include 'home.php';
                                    } elseif($_GET['page'] == 'e_gatra'){
                                        include 'e_gatra.php';
                                    } elseif($_GET['page'] == 'e_lingang'){
                                        include 'e_lingang.php';
                                    } elseif($_GET['page'] == 'e_kaka'){
                                        include 'e_kaka.php';
                                    } elseif($_GET['page'] == 'e_lapor'){
                                        include 'e_lapor.php';
                                    } elseif($_GET['page'] == 'e_gatra_news'){
                                        include 'e_gatra_news.php';
                                    } elseif($_GET['page'] == 'e_gatra_post'){
                                        include 'masukkan_berita.php';
                                    }
				                }else{include 'home.php';}
                            ?>
                        </div>
                    </div>
                </div>
        </div>
    </div>
<?php
	include 'footer.php';
?>