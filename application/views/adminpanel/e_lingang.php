<?php $this->load->view('__layouts/adminheader')?>



	<div class="row">
		<div class="col-md-12 e-lingang-content">
			<div class="content-color">
    			<div id="calendar" class="max-width-calendar">
				</div>
    			<div class="max-width-calendar">
					<div class="col-md-4"><button type="button" class="btn btn-success btn-md btn-block" style="margin-top: 15px;" data-toggle="modal" data-target="#tambahAcara"><i class="fa fa-calendar"></i> Tambah Acara</button></div>
	    			<div class="col-md-4"><button type="button" class="btn btn-warning btn-md btn-block" style="margin-top: 15px;"><i class="fa fa-pencil"></i> Edit Acara</button></div>
	    			<div class="col-md-4"><button type="button" class="btn btn-danger btn-md btn-block" style="margin-top: 15px;"><i class="fa fa-trash"></i> Hapus Acara</button></div>
    			</div>
	    	</div>
		</div>
	</div>

	<!-- Modal Popup untuk tambah Acara -->
	<div class="modal fade" id="tambahAcara" role="dialog">
    	<div class="modal-dialog modal-lg">
      		<div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Tambah Acara</h4>
		        </div>
		        <div class="modal-body">
                    <form action="" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Nama Acara</label>
                            <input type="text" name="namaacara" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Tanggal Mulai</label>
                           	<input type="date" name="tanggalmulai" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Tanggal Berakhir</label>
                           	<input type="date" name="tanggalberakhir" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                           	<input type="text" name="keterangan" class="form-control">
                        </div>
                    </form>

		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn-close" data-dismiss="modal">Tutup</button>
		        </div>
      		</div>
    	</div>

  	</div>
    <?php $this->load->view('__layouts/adminfooter')?>

  	</div>

