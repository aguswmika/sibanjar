
<?php $this->load->view('__layouts/adminheader') ?>

<div class="container">
	<div class="row">
		<div class="col-md-9">
			<div class="col-md-12 newsbox">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
			  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			  <li data-target="#myCarousel" data-slide-to="1"></li>
			  <li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>
		  
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
			  <div class="item active">

				<img src="../img/pohon.jpg"><h2 class="newsh-title">Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h2>
			  </div>
		  
			  <div class="item">
				<a href="indexpanel.php?page=e_gatra_news"><img src="../img/gunungagung.jpg"><h2 class="newsh-title">Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h2></a>
			  </div>
			  <div class="item">
				<img src="../img/banjir.jpg">

				<img src="<?php echo base_url("assets/") ?>img/pohon.jpg"><h2 class="newsh-title">Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h2>
			  </div>
		  
			  <div class="item">
				<a href="indexpanel.php?page=e_gatra_news"><img src="<?php echo base_url("assets/") ?>img/gunungagung.jpg"><h2 class="newsh-title">Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h2></a>
			  </div>
			  <div class="item">
				<img src="<?php echo base_url("assets/") ?>img/banjir.jpg">

			  </div>
			</div>
		  
			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
			  <span class="glyphicon glyphicon-chevron-left"></span>
			  <span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">
			  <span class="glyphicon glyphicon-chevron-right"></span>
			  <span class="sr-only">Next</span>
			</a>
		  </div>
				<div>
					<h3>TERKINI</h3>
				</div>
				<div class="row news-row">
					<div class="col-md-4 zero-padding">

						<img src="../img/kerja.jpg">
						<div class="back-title"><h5>Seluruh Warga Banjar Kerja Bakti Membersihkan Lingkungan</h5></div>
					</div>
					<div class="col-md-4 zero-padding border-row">
					<img src="../img/pohon.jpg">
					<div class="back-title"><h5>Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h5></div>
					</div>
					<div class="col-md-4 zero-padding">
						<img src="../img/banjir.jpg">

						<img src="<?php echo base_url("assets/") ?>img/kerja.jpg">
						<div class="back-title"><h5>Seluruh Warga Banjar Kerja Bakti Membersihkan Lingkungan</h5></div>
					</div>
					<div class="col-md-4 zero-padding border-row">
					<img src="<?php echo base_url("assets/") ?>img/pohon.jpg">
					<div class="back-title"><h5>Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h5></div>
					</div>
					<div class="col-md-4 zero-padding">
						<img src="<?php echo base_url("assets/") ?>img/banjir.jpg">

						<div class="back-title"><h5>Hujan Mengguyur Selama Lebih dari 5 Jam Membuat Air Sungai Meluap</h5></div>
					</div>
				</div>
				<div>
					<h3>BERITA LAINYA</h3>
				</div>
				<div class="news">

					<div class="col-md-3 wrap-news-img"><img src="../img/kerja.jpg"></div>

					<div class="col-md-3 wrap-news-img"><img src="<?php echo base_url("assets/") ?>img/kerja.jpg"></div>

				  	<div class="col-md-9">
				  		<h3>Seluruh Warga Banjar Kerja Bakti Membersihkan Lingkungan</h3>
				  		<h5>(Rabu, 29 November 2017)</h>
				  	</div>
				</div>
				<div class="news">

					<div class="col-md-3 wrap-news-img"><img src="../img/pohon.jpg"></div>

					<div class="col-md-3 wrap-news-img"><img src="<?php echo base_url("assets/") ?>img/pohon.jpg"></div>

				  	<div class="col-md-9">
				  		<h3>Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h3>
				  		<h5>(Senin, 27 November 2017)</h5>
				  	</div>
				</div>
				<div class="news">

					<div class="col-md-3 wrap-news-img"><img src="../img/banjir.jpg"></div>

					<div class="col-md-3 wrap-news-img"><img src="<?php echo base_url("assets/") ?>img/banjir.jpg"></div>

				  	<div class="col-md-9">
				  		<h3>Hujan Mengguyur Selama Lebih dari 5 Jam Membuat Air Sungai Meluap</h3>
				  		<h5>(Sabtu, 25 November 2017)</h5>
				  	</div>
				</div>
				<div>
					<h3>TERKINI</h3>
				</div>
				<div class="row news-row">
					<div class="col-md-4 zero-padding">

						<img src="../img/kerja.jpg">
						<div class="back-title"><h5>Seluruh Warga Banjar Kerja Bakti Membersihkan Lingkungan</h5></div>
					</div>
					<div class="col-md-4 zero-padding border-row">
						<img src="../img/pohon.jpg">
						<div class="back-title"><h5>Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h5></div>
					</div>
					<div class="col-md-4 zero-padding">
						<img src="../img/banjir.jpg">

						<img src="<?php echo base_url("assets/") ?>img/kerja.jpg">
						<div class="back-title"><h5>Seluruh Warga Banjar Kerja Bakti Membersihkan Lingkungan</h5></div>
					</div>
					<div class="col-md-4 zero-padding border-row">
						<img src="<?php echo base_url("assets/") ?>img/pohon.jpg">
						<div class="back-title"><h5>Angin Kencang Mengakibatkan Pohon Tumbang di Jalan Kubu</h5></div>
					</div>
					<div class="col-md-4 zero-padding">
						<img src="<?php echo base_url("assets/") ?>img/banjir.jpg">

						<div class="back-title"><h5>Hujan Mengguyur Selama Lebih dari 5 Jam Membuat Air Sungai Meluap</h5></div>
					</div>
				</div>	
			</div>
			<center>
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<li>
							<a href="#" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						</a>
					</li>
					</ul>
				</nav>
			</center>
		</div>
		<div class="col-md-3">
			<div class="col-md-12">
			    <div class="input-group" style="margin-top:25px ">
			     	<input type="search" class="form-control" placeholder="cari" style="border-top-left-radius: 20px;border-bottom-left-radius: 20px;">
			      	<span class="input-group-btn">
			        	<button class="btn tombol" type="button"><i class="fa fa-search"></i></button>
			      	</span>
			    </div>
			</div>
			<div class="col-md-12" style="margin-top:25px;">
				<div class="news-popular">
					<div class="pnews-box">
						<div class="pnews-title" style="font-size:20px;text-align: center;">
							Berita Terpopuler
						</div>
					</div>
					<div class="pnews-box">
						<div class="col-md-2 pnews-number"style="background-color:#ff9775;">
							1
						</div>
						<div class="col-md-10 pnews-title">
							Gunung Agung
						</div>
					</div>
					<div class="pnews-box">
						<div class="col-md-2 pnews-number" style="background-color:#ff855d;">
							2
						</div>
						<div class="col-md-10 pnews-title">
							Banjir Melanda
						</div>
					</div>
					<div class="pnews-box">
						<div class="col-md-2 pnews-number"style="background-color:#f0774f;">
							3
						</div>
						<div class="col-md-10 pnews-title">
							Pohon Tumbang
						</div>
					</div>
					<div class="pnews-box">
						<div class="col-md-2 pnews-number"style="background-color:#d86e4a;">
							4
						</div>
						<div class="col-md-10 pnews-title">
							Kerja Bakti Bersama
						</div>
					</div>
					<div class="pnews-box">
						<div class="col-md-2 pnews-number"style="background-color:#b5593b;">
							5
						</div>
						<div class="col-md-10 pnews-title">
							Lomba 17 Agustus 2017
						</div>
					</div>
				</div>
				<div class="news-popular anews">
					<div class="pnews-box">
						<div class="pnews-title" style="font-size:20px;text-align: center;">
							Artikel Terpopuler
						</div>
					</div>
					<div class="pnews-box">
						<div class="col-md-2 pnews-number"style="background-color:#ff9775;">
							1
						</div>
						<div class="col-md-10 pnews-title">
							Jenis Jenis Banten
						</div>
					</div>
					<div class="pnews-box">
						<div class="col-md-2 pnews-number"style="background-color:#ff855d;">
							2
						</div>
						<div class="col-md-10 pnews-title">
							Jenis Jenis Kulkul
						</div>
					</div>
					<div class="pnews-box">
						<div class="col-md-2 pnews-number"style="background-color:#f0774f;">
							3
						</div>
						<div class="col-md-10 pnews-title">
							Tips Tips Unik
						</div>
					</div>
					<div class="pnews-box">
						<div class="col-md-2 pnews-number"style="background-color:#d86e4a;">
							4
						</div>
						<div class="col-md-10 pnews-title">
							Cara Membuat Ketupat
						</div>
					</div>
					<div class="pnews-box">
						<div class="col-md-2 pnews-number"style="background-color:#b5593b;">
							5
						</div>
						<div class="col-md-10 pnews-title">
							Kumpulan Doa
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php $this->load->view('__layouts/adminfooter') ?>
