<?php $this->load->view('__layouts/header') ?>
<body>
	<style>
		.text-error{
			color: #F44336;
			padding: 2px;
			margin-top: 10px;
			display: inline-block;
		}
	</style>
	<div class="container-login">
		<div class="tb">
			<div class="tbc">
				<div class="login-box">
					<h1 style="color: #FFF;margin-bottom: 30px;">Masuk ke siBanjar</h1>
					<?php echo $this->session->flashdata('log') ?>
					<?php echo form_open() ?>
						<div class="form-group">
							<input type="text" class="form-control" name="user" placeholder="Username">
							<?php echo form_error('user', '<span class="text-error">', '</span>'); ?>
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="pass" placeholder="Password">
							<?php echo form_error('pass', '<span class="text-error">', '</span>'); ?>
						</div>
						<div class="form-group">
							<button class="btn btn-info btn-block" style="margin-top: 25px"><i class="fa fa-sign-in"></i> Masuk</button>
							<a href="<?php echo base_url() ?>" class="btn btn-warning btn-block" style="margin-top: 10px"><i class="fa fa-arrow-left"></i> Kembali</a>
						</div>
					<?php echo form_close() ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<?php $this->load->view('__layouts/footer') ?>