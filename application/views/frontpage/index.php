<?php $this->load->view('__layouts/header') ?>
<body>
	<section id="header">
		<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#headercustom" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand clearfix" href="#"><img src="<?php echo base_url("assets/") ?>img/Logo.png" alt=""><span>siBanjar</span></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="headercustom">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#about">Tentang</a></li>
						<li><a href="#feature">Fitur</a></li>
						<li><a href="#working">Cara Kerja</a></li>
						<li><a href="#contact">Kontak</a></li>
						<li id="sign-in"><a href="<?php echo base_url('login') ?>">Masuk <i class="fa fa-sign-in"></i></a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
	</section>
	<section id="hero">
		<div class="tb">
			<div class="tbc">
				<div class="img-logo">
					<img src="<?php echo base_url("assets/") ?>img/logo2.png" alt="">
				</div>
				<h1>
					Sukseskan Banjar Anda dengan <br>bergabung dengan <span style="color: #b5593b;">siBanjar</span>!
				</h1>
				<h4>
					Dapatkan layanan terbaik dari kami untuk Banjar Anda
				</h4>
				<a href="#contact" class="btn btn-default scroll">Gabung Sekarang!</a>	
			</div>
		</div>
	</section>
	<section id="about">
		<div class="container">
			<center>
				<div class="contain-title" style="margin-bottom: 80px;">		
					<h1 class="title scroll">Apa itu siBanjar?</h1>
					<h3 class="title-small scroll">Yuk! Kenalan dulu dengan siBanjar</h3>
				</div>
				<p class="scroll" style="font-weight:200; width:75%;margin-bottom: 40px;">
					siBanjar merupakan pengaplikasian konsep <i>smartcity</i> dalam lingkup yang lebih kecil, digunakan untuk membantu menyelesaikan masalah-masalah yang terjadi di lingkungan banjar.
				</p>
			</center>
		</div>
	</section>
	<section id="feature">
		<div class="triangle"></div>
		<div class="container">
			<center> 
				<div class="contain-title">
					<h1 class="title scroll">Fitur siBanjar</h1>
					<h3 class="title-small scroll">siBanjar punya 4 fitur utama</h3>
				</div>
				<div class="row scroll">
					<div class="col-md-3">
						<div class="img-feature">
							<img src="<?php echo base_url("assets/") ?>img/kaka.png" alt="">
						</div>
						<h3 class="title-feature">e-KaKa</h3>
						<p>Mengetahui informasi tentang data warga Banjar</p>
					</div>
					<div class="col-md-3">
						<div class="img-feature">
							<img src="<?php echo base_url("assets/") ?>img/kalender.png" alt="">
						</div>
						<h3 class="title-feature">e-Lingang</h3>
						<p>Mengetahui tanggal penting dan jadwal kegiatan di Banjar</p>
					</div>
					<div class="col-md-3">
						<div class="img-feature">
							<img src="<?php echo base_url("assets/") ?>img/gatra.png" alt="">
						</div>
						<h3 class="title-feature">e-Gatra</h3>
						<p>Mengetahui berita terbaru dan informasi yang edukatif </p>
					</div>
					<div class="col-md-3">
						<div class="img-feature">
							<img src="<?php echo base_url("assets/") ?>img/lapor.png" alt="">
						</div>
						<h3 class="title-feature">e-Lapor</h3>
						<p>Melaporkan masalah yang terjadi di sekitar lingkungan Banjar</p>
					</div>
				</div>
			</center>
		</div>
	</section>
	<section id="working">
		<div class="container">
			<center>
				<div class="contain-title">
					<h1 class="title scroll">Bagaimana siBanjar Bekerja?</h1>
					<h3 class="title-small scroll" style="color: #eaeaea;">Yuk! Simak informasi di bawah ini</h3>
				</div>
				<div class="row">
					<div class="col-md-3 scroll">
						<div class="work">
							<i class="fa fa-user-plus"></i>
							<h4>Daftarkan Warga</h4>
							<p>Kelian Banjar dapat mendaftarkan warga ke dalam sistem.</p>
						</div>
					</div>
					<div class="col-md-3 scroll">
						<div class="work">
							<i class="fa fa-id-card"></i>
							<h4>Dapatkan Akun</h4>
							<p>Gunakan akun untuk masuk ke sistem dan mulai jelajahi.</p>
						</div>
					</div>
					<div class="col-md-3 scroll">
						<div class="work">
							<i class="fa fa-search"></i>
							<h4>Jelajahi Fitur</h4>
							<p>Menjelajah siBanjar lebih mudah dengan fitur yang ada</p>
						</div>
					</div>
					<div class="col-md-3 scroll">
						<div class="work">
							<i class="fa fa-globe"></i>
							<h4>Dapatkan Informasi</h4>
							<p>Nikmati mudahnya mendapatkan informasi di siBanjar</p>
						</div>
					</div>
				</div>
			</center>
		</div>
	</section>
	
	<section id="contact">
		<div class="container">
			<center>
				<div class="contain-title">
					<h1 class="title scroll">Hubungi Kami</h1>
					<h3 class="title-small scroll" style="color:#eaeaea;">Masih kurang jelas? Silahkan hubungi kami</h3>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-contact scroll">
							<form action="">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" class="form-control">
								</div>
								<div class="form-group">
									<label>Email</label>
									<input type="email" class="form-control">
								</div>
								<div class="form-group">
									<label>Pesan</label>
									<textarea class="form-control" rows="5"></textarea>
								</div>
								<div class="form-group">
									<button class="btn btn-info btn-block"><i class="fa fa-send"></i> Kirim</button>
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-6">
						<div class="desc-contact scroll">
							<h2>Hubungi kami melewati platform lain :</h2>
							<span><i class="fa fa-facebook"></i> siBanjar</span>
							<span><i class="fa fa-instagram"></i> siBanjar</span>
							<span><i class="fa fa-envelope"></i> info@sibanjar.org</span>
							<span><i class="fa fa-phone"></i> (0361) - 14045</span>
						</div>
					</div>
				</div>
			</center>
		</div>
	</section>
	<section id="footer">
		Copyright &copy; 2017; Developed with <i class="fa fa-heart" style="color: red;"></i> by Kelompok 3
	</section>
<?php $this->load->view('__layouts/footer') ?>
<script>
		window.sr = ScrollReveal();
		sr.reveal('.scroll');
		$(document).ready(function(){
			$('.navbar-right a').click(function(){
				var anchor = $(this);	 
				$('html, body').stop().animate({scrollTop: $(anchor.attr('href')).offset().top - 50}, 1500);
				return false;	
			});
		})
	</script>