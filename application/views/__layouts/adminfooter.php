    <script src="<?php echo base_url('assets/') ?>js/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/') ?>js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            function resizewin(){
                if ($(window).width() <= 768) {
                    $('.collapsed').attr('data-toggle', 'untoggle');
                    $('#aside').css({ 'display': 'none', 'width': '0px' })
                    $('#content').css({ 'padding': '66px 0 0 0px' });
                    $('.overlay').css({ 'display': 'none' });
                    $(this).attr('data-toggle', 'untoggle');
                } else {
                    $('#aside').css({ 'display': 'block', 'width': '250px'});
                    $('#content').css({ 'padding': '66px 0 0 250px' })
                    $('.overlay').css({ 'display': 'none',});
                    $(this).attr('data-toggle', 'toggle');
                }
            }
            resizewin();
            $(window).resize(function(){
               resizewin();
            })
            $('.collapsed').click(function(){
                if($(this).attr('data-toggle') == 'untoggle'){
                    $('#aside').css({'display': 'block', 'width': '250px' });
                    if ($(window).width() <= 768) {
                        $('.overlay').css({ 'display': 'block' });
                        $('#content').css({ 'padding': '66px 0 0 0px' });
                    }else{
                        $('#content').css({ 'padding': '66px 0 0 250px' });
                    }
                    $(this).attr('data-toggle', 'toggle');
                }else{
                    $('#aside').css({ 'display': 'none', 'width': '0px' })
                    $('#content').css({ 'padding': '66px 0 0 0px' });
                    $('.overlay').css({ 'display': 'none' });
                    $(this).attr('data-toggle', 'untoggle');
                }
            });
        });

    </script>
</body>
</html>