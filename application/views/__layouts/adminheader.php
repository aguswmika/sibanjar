<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="shorcut icon" href="<?php echo base_url('assets/') ?>img/logo.png">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/style-admin.css">
</head>
<body>
    <section id="header">
        <nav class="navbar navbar-default navbar-fixed-top" id="navbar-custom">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand clearfix" href="#">
                        <img src="<?php echo base_url('assets/') ?>img/Logo.png" alt="">
                        <span>siBanjar - <?php echo get_roles() ?></span>
                    </a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
    
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <?php if(get_user()->hak_akses != 1){ ?>
                             <li>
                                <a href="<?php echo base_url('control-panel/profile/'.get_user()->nik) ?>">Change Profile</a>
                            </li>
                        <?php } ?>
                        <li id="sign-in">
                            <a href="<?php echo base_url('logout') ?>">Log Out
                                <i class="fa fa-sign-out"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
        <aside id="aside">
            <ul id="menu">
                <div class="users">
                    <div class="user-img">
                        <?php if(!empty(get_user()->photo)){ ?>
                            <img class="img-responsive" alt="Foto Profil" src="<?php echo base_url(get_user()->photo) ?>">
                        <?php } ?>
                    </div>
                    <div class="user-name">
                        Hi, <?php 
                                $nama = explode(' ', get_user()->nama); 
                                $jml  = count($nama);
                                if(count($nama) >= 3) $jml = 3;
                                for ($i=0; $i < $jml; $i++) echo $nama[$i]." ";
                            ?>
                    </div>
                </div>
                <li><a <?php echo ($this->uri->segment(2) == '') ? 'class="active"' : '' ?> href="<?php echo base_url('control-panel') ?>"><i class="fa fa-home"></i> Dashboard</a></li>
                <?php if(get_user()->hak_akses == 1){ ?> 
                    <li><a <?php echo ($this->uri->segment(2) == 'banjar' || $this->uri->segment(2) == 'kaka') ? 'class="active"' : '' ?> href="<?php echo base_url('control-panel/banjar') ?>"><i class="fa fa-bank"></i> e-Banjar</a></li>
                    <li><a <?php echo ($this->uri->segment(2) == 'kategori') ? 'class="active"' : '' ?> href="<?php echo base_url('control-panel/kategori') ?>"><i class="fa fa-tags"></i> e-Kategori</a></li>
                <?php } ?>
                <?php if(get_user()->hak_akses != 1){ ?> 
                    <li><a <?php echo ($this->uri->segment(2) == 'gatra') ? 'class="active"' : '' ?> href="<?php echo base_url('control-panel/gatra') ?>"><i class="fa fa-newspaper-o"></i> e-Gatra</a></li>
                    <li><a <?php echo ($this->uri->segment(2) == 'lingang') ? 'class="active"' : '' ?> href="<?php echo base_url('control-panel/lingang') ?>"><i class="fa fa-calendar"></i> e-Lingang</a></li>
                    <li><a <?php echo ($this->uri->segment(2) == 'kaka') ? 'class="active"' : '' ?> href="<?php echo base_url('control-panel/kaka') ?>"><i class="fa fa-vcard"></i> e-KaKa</a></li>
                    <li><a <?php echo ($this->uri->segment(2) == 'lapor') ? 'class="active"' : '' ?> href="<?php echo base_url('control-panel/lapor') ?>"><i class="fa fa-bullhorn"></i> e-Lapor</a></li>
                <?php } ?>
            </ul>
        </aside>
        <div class="overlay"></div>
    </section>