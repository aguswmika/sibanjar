<?php     
defined('BASEPATH') OR exit('No direct script access allowed');

class Tb_lapor extends CI_Model {
	public function count(){
		return $this->db->join('tb_cat_lapor', 'tb_cat_lapor.id = tb_lapor.id_cat_lapor')
						->join('tb_user', 'tb_user.nik = tb_lapor.nik')
						->join('tb_kaka', 'tb_kaka.nokk = tb_user.nokk')
						->where(['tb_kaka.id_banjar' => get_user()->id_banjar])
						->get('tb_lapor')->num_rows();
	}
	public function getAllByBanjar($id){
		$this->db->where(['nik' => $id]);
		return $this->db->select('tb_lapor.*, tb_cat_lapor.judul as lp_judul')
						->join('tb_cat_lapor', 'tb_cat_lapor.id = tb_lapor.id_cat_lapor')
						->get('tb_lapor')->result();
	}

	public function getSingle($id){
		$this->db->where(['tb_lapor.id' => $id]);
		return $this->db->select('tb_lapor.*, tb_cat_lapor.judul as lp_judul')
						->join('tb_cat_lapor', 'tb_cat_lapor.id = tb_lapor.id_cat_lapor')
						->get('tb_lapor')->row();
	}

	public function getAllById($id){
		$this->db->where(['nik' => $id]);
		return $this->db->select('tb_lapor.*, tb_cat_lapor.judul as lp_judul')
						->join('tb_cat_lapor', 'tb_cat_lapor.id = tb_lapor.id_cat_lapor')
						->get('tb_lapor')->result();
	}
	public function getAll(){
		return $this->db->select('tb_lapor.*, tb_cat_lapor.judul as lp_judul, tb_kaka.id_banjar')
						->join('tb_cat_lapor', 'tb_cat_lapor.id = tb_lapor.id_cat_lapor')
						->join('tb_user', 'tb_user.nik = tb_lapor.nik')
						->join('tb_kaka', 'tb_kaka.nokk = tb_user.nokk')
						->where(['tb_kaka.id_banjar' => get_user()->id_banjar])
						->get('tb_lapor')->result();
	}

	public function store(){
		$config['upload_path']          = './uploads/img';
		$config['allowed_types']        = 'png|jpg';
		$config['max_size']             = 5124;
		$config['encrypt_name']			= TRUE;

		$this->upload->initialize($config);

		if($this->upload->do_upload('gambar')){
			$dir 	= 'uploads/img/'.$this->upload->data('file_name');
			$desc   = (empty($this->input->post('deskripsi'))) ? NULL : $this->input->post('deskripsi');
			$data 	= [
				'id'			=> autoNum('tb_lapor', 'id', 'LPR'),
				'judul'			=> $this->input->post('nama'),
				'deskripsi'		=> $desc,
				'id_cat_lapor'  => $this->input->post('kategori'),
				'nik'			=> get_user()->nik,
				'gambar'		=> $dir,
				'status'		=> 'belum ditanggapi'
			];

			if($this->db->insert('tb_lapor', $data)){
				$this->session->set_flashdata('log', msg('Data berhasil ditambahkan!'));	
				return true;
			}else{
				$this->session->set_flashdata('log', msg('Data gagal ditambahkan!', 'danger'));	
				return false;
			}
		}else{
			$this->session->set_flashdata('log', msg($this->upload->display_errors(), 'danger'));
			return false;
		}
	}

	public function status($id){
		$data = [
			'status' => $this->input->post('status')
		];
		$this->db->where(['id' => $id]);
		return $this->db->update('tb_lapor', $data);
	}

	public function delete($id){
		return $this->db->delete('tb_lapor', ['id' => $id]);
	}
}