<?php     
defined('BASEPATH') OR exit('No direct script access allowed');

class Tb_kategori extends CI_Model {	
	public function getAllGatra(){
		return $this->db->order_by('id', 'ASC')->get('tb_cat_gatra')->result();
	}

	public function getAllLapor(){
		return $this->db->order_by('id', 'ASC')->get('tb_cat_lapor')->result();
	}

	public function getSingle($tipe, $id){
		$this->db->where(['id' => $id]);
		return $this->db->get('tb_cat_'.$tipe)->row();
	}

	public function store($tipe){
		$pfx  = ($tipe == 'gatra') ? 'CGT' : 'CLP';
		$desc = (empty($this->input->post('deskripsi'))) ? NULL : $this->input->post('deskripsi');
		$data = [
			'id'		=> autoNum('tb_cat_'.$tipe, 'id', $pfx),
			'judul' 	=> $this->input->post('nama'),
			'deskripsi' => $desc
		];

		return $this->db->insert('tb_cat_'.$tipe, $data);
	}

	public function update($tipe, $id){
		$pfx  = ($tipe == 'gatra') ? 'CGT' : 'CLP';
		$desc = (empty($this->input->post('deskripsi'))) ? NULL : $this->input->post('deskripsi');
		$data = [
			'judul' 	=> $this->input->post('nama'),
			'deskripsi' => $desc
		];
		$this->db->where(['id' => $id]);
		return $this->db->update('tb_cat_'.$tipe, $data);
	}

	public function delete($tipe, $id){
		return $this->db->delete('tb_cat_'.$tipe, ['id' => $id]);
	}
}