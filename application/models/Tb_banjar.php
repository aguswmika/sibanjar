<?php     
defined('BASEPATH') OR exit('No direct script access allowed');

class Tb_banjar extends CI_Model {
	public function count(){
		if(get_user()->hak_akses == 1)
			return $this->db->get('tb_banjar')->num_rows();
		else return $this->db->where(['id' => get_user()->id_banjar])->get('tb_banjar')->num_rows();
	}

	public function getSingle($id){
		$this->db->where(['id' => $id]);
		return $this->db->get('tb_banjar')->row();
	}

	public function getAll(){
		return $this->db->select('tb_banjar.*, (SELECT count(nik) FROM tb_user INNER JOIN tb_kaka ON tb_kaka.nokk = tb_user.nokk WHERE tb_kaka.id_banjar = tb_banjar.id) as jml')->order_by('id', 'ASC')->get('tb_banjar')->result();
	}

	public function store(){
		$config['upload_path']          = './uploads/img';
		$config['allowed_types']        = 'png|jpg';
		$config['max_size']             = 500;
		$config['encrypt_name']			= TRUE;

		$this->upload->initialize($config);

		if($this->upload->do_upload('gambar')){
			$dir 	= 'uploads/img/'.$this->upload->data('file_name');
			$desc   = (empty($this->input->post('deskripsi'))) ? NULL : $this->input->post('deskripsi');
			$data 	= [
				'id'		=> autoNum('tb_banjar', 'id', 'BJR'),
				'nama' 		=> $this->input->post('nama'),
				'deskripsi' => $desc,
				'alamat'    => $this->input->post('alamat'),
				'gambar'	=> $dir
			];

			return $this->db->insert('tb_banjar', $data);
		}else{
			$this->session->set_flashdata('log', msg($this->upload->display_errors(), 'danger'));
			return false;
		}
	}

	public function update($id){
		$config['upload_path']          = './uploads/img';
		$config['allowed_types']        = 'png|jpg';
		$config['max_size']             = 500;
		$config['encrypt_name']			= TRUE;

		$this->upload->initialize($config);

		$this->upload->do_upload('gambar');
		$dir 	= 'uploads/img/'.$this->upload->data('file_name');
		$desc   = (empty($this->input->post('deskripsi'))) ? NULL : $this->input->post('deskripsi');
		$data 	= [
			'nama' 		=> $this->input->post('nama'),
			'deskripsi' => $desc,
			'alamat'    => $this->input->post('nama'),
		];

		if(!empty($this->upload->data('file_name'))){
			array_push($data, ['gambar'	=> $dir]);
			
			$old_photo = $this->getSingle($id)->gambar;

			if(file_exists($old_photo)){
				unlink($old_photo);
			}
		}
		
		$this->db->where(['id' => $id]);
		return $this->db->update('tb_banjar', $data);
	}

	public function delete($id){
		return $this->db->delete('tb_banjar', ['id' => $id]);
	}
}