<?php     
defined('BASEPATH') OR exit('No direct script access allowed');

class Tb_lingang extends CI_Model {
	public function count(){
		return $this->db->join('tb_user', 'tb_user.nik = tb_lingang.nik')
						->join('tb_kaka', 'tb_kaka.nokk = tb_user.nokk')
						->where(['tb_kaka.id_banjar' => get_user()->id_banjar])
						->get('tb_lingang')->num_rows();
	}

	public function getSingle($id){
		$this->db->where(['id' => $id, 'tb_kaka.id_banjar' => get_user()->id_banjar]);
		return $this->db->select('tb_lingang.tgl_awal as start, tb_lingang.tgl_akhir as end, tb_lingang.deskripsi as deskripsi, tb_lingang.judul as title, tb_lingang.id as id')->join('tb_user', 'tb_user.nik = tb_lingang.nik')->join('tb_kaka', 'tb_kaka.nokk = tb_user.nokk')->get('tb_lingang')->row();
	}

	public function getAll(){
		$this->db->where(['tb_kaka.id_banjar' => get_user()->id_banjar]);
		return $this->db->select('tb_lingang.tgl_awal as start, tb_lingang.tgl_akhir as end, tb_lingang.judul as title, tb_lingang.id as id')->join('tb_user', 'tb_user.nik = tb_lingang.nik')->join('tb_kaka', 'tb_kaka.nokk = tb_user.nokk')->get('tb_lingang')->result();
	}

	public function store(){
		$desc = (empty($this->input->post('deskripsi'))) ? NULL : $this->input->post('deskripsi');
		$data = [
			'id'		=> autoNum('tb_lingang', 'id', 'LIG'),
			'judul' 	=> $this->input->post('nama'),
			'deskripsi' => $desc,
			'nik'		=> get_user()->nik,
			'tgl_awal'  => $this->input->post('start'),
			'tgl_akhir' => $this->input->post('end')
		];

		return $this->db->insert('tb_lingang', $data);
	}

	public function update($id){
		$desc = (empty($this->input->post('deskripsi'))) ? NULL : $this->input->post('deskripsi');
		$data = [
			'judul' 	=> $this->input->post('nama'),
			'deskripsi' => $desc,
			'tgl_awal'  => $this->input->post('start'),
			'tgl_akhir' => $this->input->post('end')
		];

		$this->db->where(['id' => $id]);
		return $this->db->update('tb_lingang', $data);
	}

	public function delete($id){
		return $this->db->delete('tb_lingang', ['id' => $id]);
	}
}