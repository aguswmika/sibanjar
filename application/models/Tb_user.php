<?php     
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as xlsx;

class Tb_user extends CI_Model {

	public function count(){
		if(get_user()->hak_akses == 1)
			return $this->db->get('tb_user')->num_rows();
		else return $this->db->select('tb_user.*, tb_kaka.id_banjar as id_banjar')
							 ->join('tb_kaka', 'tb_kaka.nokk = tb_user.nokk')
							 ->where(['tb_kaka.id_banjar' => get_user()->id_banjar])
							 ->get('tb_user')->num_rows();
	}

	public function getSingleByToken($token){
		$this->db->where(['token' => $token]);
		return $this->db->get('tb_user')->row();
	}

	public function getSingleById($id){
		$this->db->where(['nik' => $id]);
		return $this->db->get('tb_user')->row();
	}

	public function getSingleByKaka(){
		$kaka = $this->db->select('tb_kaka.*')->where(['id_banjar' => get_user()->id_banjar, 'nokk' => get_user()->nokk])->get('tb_kaka')->result();
		$data = [];
		foreach ($kaka as $value) {
			$data[$value->nokk] = [
				'kepala'	=> $this->getSingleById($value->id_kepala),
				'data' 		=> $this->db->where(['nokk' => $value->nokk])->get('tb_user')->result()
			];
		}

		return $data;
	}

	public function getKelianByBanjar($id){
		return $this->db->select('tb_user.*, tb_kaka.id_banjar')
						->join('tb_kaka', 'tb_kaka.nokk = tb_user.nokk')
						->where(['tb_kaka.id_banjar' => $id, 'tb_user.hak_akses' => 2])
						->get('tb_user')
						->row();
	}

	public function getAllById($id){
		$kaka = $this->db->select('tb_kaka.*')->where(['id_banjar' => $id])->order_by('nokk', 'ASC')->get('tb_kaka')->result();
		$data = [];
		foreach ($kaka as $value) {
			$data[$value->nokk] = [
				'kepala'	=> $this->getSingleById($value->id_kepala),
				'data' 		=> $this->db->where(['nokk' => $value->nokk])->get('tb_user')->result()
			];
		}

		return $data;
	}

	public function getAllByBanjar($id){
		return $this->db->select('tb_user.*, tb_kaka.id_banjar')
						->join('tb_kaka', 'tb_kaka.nokk = tb_user.nokk')
						->where(['tb_kaka.id_banjar' => $id])
						->get('tb_user')
						->result();
	}

	public function storeExcel($id){
		
		if($this->db->where(['id' => $id])->get('tb_banjar')->num_rows() == 1){
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'xls|xlsx';
			$config['max_size']             = 5120;
			$config['encrypt_name']			= TRUE;

			$this->upload->initialize($config);

			if($this->upload->do_upload('filename')){

				$dir = './uploads/'.$this->upload->data('file_name');
			
				$render = new xlsx();
				$spreadsheet = $render->load($dir);

				$sheetData = $spreadsheet->getActiveSheet()->toArray();

				$data 	= [];
				$data2 	= [];
				$nokk   = [];
				$alamat = [];
				$kepala = [];
				foreach ($sheetData as $no => $key) {
					if($no > 0){
						if(!empty($key[0])){
							$this->db->where(['nik' => $key[0]]);

							if($this->db->get('tb_user')->num_rows() == 0){
								if($key[5] == 'l')
									$key[5] = 'laki-laki';
								else
									$key[5] = 'perempuan';

								if($key[7] == 'bk')
									$key[7] = 'belum kawin';
								else
									$key[7] = 'kawin';

								$date = date('Y-m-d', strtotime($key[3]));

								$ins_data =  [
									'nik'  		=> $key[0],
									'nama' 		=> $key[1],
									'tmp_lahir'	=> $key[2],
									'tgl_lahir' => $date,
									'agama'		=> $key[4],
									'jk'		=> $key[5],
									'alamat'	=> $key[6],
									'status'	=> $key[7],
									'nokk'		=> $key[8],
									'password'  => md5($date),
									'hak_akses'	=> 3,
								];

								if($key[9] == 'y'){
									$kepala[$key[8]] = $key[0];
								}

								array_push($nokk, $key[8]);
								array_push($alamat, $key[10]);
								array_push($data, $ins_data);
							}
						}
					}
				}
				$nokk = array_unique($nokk);
				$alamat = array_unique($alamat);
				foreach ($nokk as $key => $value) {
					$this->db->where(['nokk' => $key[0]]);
					if($this->db->get('tb_kaka')->num_rows() == 0){
						$ins_data = [
							'nokk' 		=> $value,
							'id_kepala' => $kepala[$value],
							'id_banjar' => $id,
							'alamat' 	=> $alamat[$key]
						];
					}

					array_push($data2, $ins_data);
				}
				
				
				if(file_exists($dir)){
					unlink($dir);
				}
				if(count($data2) > 0 && count($data) > 0){
					if($this->db->insert_batch('tb_kaka', $data2))
						if($this->db->insert_batch('tb_user', $data))
							$this->session->set_flashdata('log', msg(count($data).' data berhasil dimasukan!', 'success'));
						else $this->session->set_flashdata('log', msg('Gagal memasukan data!', 'danger'));	
					else
						$this->session->set_flashdata('log', msg('Gagal memasukan data!', 'danger'));
						return false;
				}else{
					$this->session->set_flashdata('log', msg('Data penduduk sudah ada!', 'warning'));
					return false;
				}
			}else{
				$this->session->set_flashdata('log', msg('Error dalam file, pastikan file sudah benar!', 'warning'));
				return false;
			}
		}else{
			$this->session->set_flashdata('log', msg('Banjar belum terdaftarkan!', 'warning'));
			return false;
		};
	}

	public function storeKelian($id){
		$old_kelian = $this->getKelianByBanjar($id);
		$kelian	= $this->input->post('kelian');
		

		if(count($old_kelian) == 1){
			$this->db->where(['nik' => $old_kelian->nik]);
			$this->db->update('tb_user', ['hak_akses' => 3]);
		}

		$this->db->where(['nik' => $kelian]);
		if($this->db->update('tb_user', ['hak_akses' => 2]))
			$this->session->set_flashdata('log', msg('Berhasil menambahkan kelian!'));
		else
			$this->session->set_flashdata('log', msg('Error menambahkan kelian!', 'danger'));
	}

	public function delete($id){
		return $this->db->delete('tb_kaka', ['nokk' => $id]);
	}

	public function profile($id){
		$config['upload_path']          = './uploads/img';
		$config['allowed_types']        = 'png|jpg';
		$config['max_size']             = 500;
		$config['encrypt_name']			= TRUE;

		$this->upload->initialize($config);

		$this->upload->do_upload('gambar');
		$dir 	= 'uploads/img/'.$this->upload->data('file_name');
		$desc   = (empty($this->input->post('deskripsi'))) ? NULL : $this->input->post('deskripsi');
		if(!empty($this->input->post('pass')) || !empty($this->upload->data('file_name'))){
			if(!empty($this->input->post('pass')))
				$data['password'] = md5($this->input->post('pass'));
			if(!empty($this->upload->data('file_name')))
				$data['photo'] = $dir;

			$this->db->where(['nik' => $id]);
			return $this->db->update('tb_user', $data);
		}else{
			return false;
		}
	}

	//start logging

	public function logging(){
		$user  = $this->input->post('user');
		$pass  = $this->input->post('pass');


		$this->db->where([
			'nik' 		=> $user,
			'password'	=> md5($pass)
		]);

		$res = $this->db->get('tb_user');

		if($res->num_rows() == 1){
			$token =  bin2hex(openssl_random_pseudo_bytes(16));
			$this->db->where(['nik' => $res->row()->nik]);
			$this->db->update('tb_user', ['token' => $token]);

			$config = [
				'token' => $token
			];

			$this->session->set_userdata($config);

			return true;
		}else{
			$this->db->where([
				'nik' 		=> $user,
				'password'	=> md5($pass)
			]);

			$res = $this->db->get('tb_admin');

			if($res->num_rows() == 1){
				$token =  bin2hex(openssl_random_pseudo_bytes(16));
				$this->db->where(['nik' => $res->row()->nik]);
				$this->db->update('tb_admin', ['token' => $token]);

				$config = [
					'token' => $token
				];

				$this->session->set_userdata($config);

				return true;
			}else
				return false;
		}
	}

	public function logout(){
		$id = $this->session->token;
		if(get_user()->hak_akses == 1){
			$this->db->where(['token' => $id]);
			$this->db->update('tb_admin', ['token' => '']);
		}else{
			$this->db->where(['token' => $id]);
			$this->db->update('tb_user', ['token' => '']);
		}
		$config = [
			'token'
		];

		$this->session->unset_userdata($config);
	}
	//end logging
}
