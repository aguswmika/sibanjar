<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// start lingang
$route['control-panel/lingang'] = 'lingang/index';
$route['control-panel/lingang/insert'] = 'lingang/insert';
$route['control-panel/lingang/(:any)/edit'] = 'lingang/edit/$1';
$route['control-panel/lingang/(:any)/delete'] = 'lingang/delete/$1';
$route['control-panel/lingang/event'] = 'lingang/event';
// end lingang
$route['control-panel'] = 'dashboard/index';
$route['control-panel/gatra'] = 'gatra/index';
// start kaka
$route['control-panel/kaka'] = 'kaka/index';
$route['control-panel/kaka/(:any)'] = 'kaka/index/$1';
$route['control-panel/kaka/(:any)/delete'] = 'kaka/delete/$1';
$route['control-panel/kaka/(:any)/excel'] = 'kaka/excel/$1';
$route['control-panel/kaka/(:any)/kelian'] = 'kaka/kelian/$1';
// end kaka
//start banjar
$route['control-panel/banjar'] = 'banjar/index';
$route['control-panel/banjar/insert'] = 'banjar/insert';
$route['control-panel/banjar/(:any)/edit'] = 'banjar/edit/$1';
$route['control-panel/banjar/(:any)/delete'] = 'banjar/delete/$1';
//end banjar
//start lapor
$route['control-panel/lapor'] = 'lapor/index';
$route['control-panel/lapor/show'] = 'lapor/show';
$route['control-panel/lapor/(:any)/status'] = 'lapor/status/$1';
$route['control-panel/lapor/(:any)/delete'] = 'lapor/delete/$1';
//end lapor
//start kategori
$route['control-panel/kategori'] = 'kategori/index';
$route['control-panel/kategori/([a-z]+)/insert'] = 'kategori/insert/$1';
$route['control-panel/kategori/([a-z]+)/(:any)/delete'] = 'kategori/delete/$1/$2';
$route['control-panel/kategori/([a-z]+)/(:any)/edit'] = 'kategori/edit/$1/$2';
//end kategori
$route['control-panel/profile/([0-9]+)'] = 'dashboard/profile/$1';
$route['login'] = 'login/index';
$route['logout'] = 'login/logout';
$route['default_controller'] = 'frontpage';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;