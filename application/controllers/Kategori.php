<?php     
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		check_access();

		if(get_user()->hak_akses > 1){
			redirect('control-panel');
		}
		$this->load->model('tb_kategori');
	}

	public function index(){
		$data = [
			'title'  	=> 'e-Kategori | siBanjar - '.get_roles().' Panel',
			'cat_gatra' => $this->tb_kategori->getAllGatra(),
			'cat_lapor' => $this->tb_kategori->getAllLapor()
		];

		$this->load->view('adminpanel/e_kategori/index', $data);
	}

	public function insert($tipe){
		if($tipe == 'gatra' || $tipe == 'lapor'){
			$data = [
				'title' => 'Tambahkan Data '.ucfirst($tipe).' | e-Kategori | siBanjar - '.get_roles().' Panel',
			];

			$rules = [
				[
					'field' => 'nama',
					'label'	=> 'Nama',
					'rules'	=> 'trim|max_length[255]|min_length[3]|xss_clean|required'
				],
				[
					'field' => 'deskripsi',
					'label'	=> 'Deskripsi',
					'rules'	=> 'min_length[3]|xss_clean'
				],
			];

			$this->form_validation->set_rules($rules);

			if($this->form_validation->run()){
				if($this->tb_kategori->store($tipe)){
					$this->session->set_flashdata('log', msg('Data berhasil ditambahkan!'));	
				}else{
					$this->session->set_flashdata('log', msg('Data gagal ditambahkan!', 'danger'));	
				}

				redirect('control-panel/kategori/'.$tipe.'/insert');
			}else{
				$this->load->view('adminpanel/e_kategori/insert', $data);
			}
		}else{
			show_404();
		}
	}

	public function edit($tipe, $id){
		if($tipe == 'gatra' || $tipe == 'lapor'){
			$data = [
				'title' => 'Edit Data '.ucfirst($tipe).' | e-Kategori | siBanjar - '.get_roles().' Panel',
				'data'	=> $this->tb_kategori->getSingle($tipe, $id)
			];

			$rules = [
				[
					'field' => 'nama',
					'label'	=> 'Nama',
					'rules'	=> 'trim|max_length[255]|min_length[3]|xss_clean|required'
				],
				[
					'field' => 'deskripsi',
					'label'	=> 'Deskripsi',
					'rules'	=> 'min_length[3]|xss_clean'
				],
			];

			$this->form_validation->set_rules($rules);

			if($this->form_validation->run()){
				if($this->tb_kategori->update($tipe, $id)){
					$this->session->set_flashdata('log', msg('Data berhasil ditambahkan!'));	
				}else{
					$this->session->set_flashdata('log', msg('Data gagal ditambahkan!', 'danger'));	
				}

				redirect('control-panel/kategori');
			}else{
				$this->load->view('adminpanel/e_kategori/edit', $data);
			}
		}else{
			show_404();
		}
	}

	public function delete($tipe, $id){
		if($tipe == 'gatra' || $tipe == 'lapor'){
			if($this->tb_kategori->delete($tipe, $id))
				$this->session->set_flashdata('log', msg('Data berhasil dihapus!'));	
			else
				$this->session->set_flashdata('log', msg('Data gagal dihapus!', 'danger'));	
			
			redirect('control-panel/kategori');
		}else{
			redirect('control-panel/kategori');
		}
	}
}