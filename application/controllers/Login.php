<?php     
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('tb_user');
	}

	public function index(){
		check_login();
		$data = [
			'title' => 'Login | siBanjar - Sistem Informasi Banjar'
		];

		$config = [
			[
				'field' => 'user',
				'label' => 'Username',
				'rules' => 'trim|required|xss_clean'
			],
			[
				'field' => 'pass',
				'label' => 'Password',
				'rules' => 'min_length[5]|max_length[50]|trim|required|xss_clean'
			]
		];
		$this->form_validation->set_rules($config);

		if($this->form_validation->run() == true){
			if($this->tb_user->logging()){
				redirect('control-panel');
			}else{
				$this->session->set_flashdata('log', msg('Username atau Password salah!', 'warning'));
				redirect('login');
			}
		}else{
			$this->load->view('frontpage/login', $data);
		}
	}

	public function logout(){
		$this->tb_user->logout();
		$this->session->set_flashdata('log', msg('Anda telah logout', 'info'));
		redirect('login');
	}
}