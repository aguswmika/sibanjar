<?php     
defined('BASEPATH') OR exit('No direct script access allowed');

class Lapor extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		check_access();

		if(get_user()->hak_akses == 1){
			redirect('control-panel');
		}

		$this->load->model('tb_lapor');
		$this->load->model('tb_kategori');
	}

	public function index(){

		$rules = [
			[
				'field' => 'nama',
				'label'	=> 'Nama',
				'rules'	=> 'trim|max_length[255]|min_length[3]|xss_clean|required'
			],
			[
				'field' => 'deskripsi',
				'label'	=> 'Deskripsi',
				'rules'	=> 'min_length[3]|xss_clean'
			],
			[
				'field' => 'kategori',
				'label'	=> 'Kategori',
				'rules'	=> 'trim|xss_clean|required'
			]
		];

		$this->form_validation->set_rules($rules);

		if($this->form_validation->run()){
			$this->tb_lapor->store();

			redirect('control-panel/lapor');
		}else{
			if(get_user()->hak_akses == 3){
				$data = [
					'title'  => 'e-Lapor | siBanjar - '.get_roles().' Panel',
					'lapor'  => $this->tb_lapor->getAllById(get_user()->nik),
					'cat'	 => $this->tb_kategori->getAllLapor()
				];
				$this->load->view('adminpanel/e_lapor/index', $data);
			}else {
				$data = [
					'title'  => 'e-Lapor | siBanjar - '.get_roles().' Panel',
					'lapor'  => $this->tb_lapor->getAll(),
				];
				$this->load->view('adminpanel/e_lapor/index2', $data);
			}
		}
	}

	public function show(){
		header('Content-Type: application/json');
		$id = $this->input->get('id');
		
		echo json_encode($this->tb_lapor->getSingle($id));
	}

	public function status($id){
		if($this->tb_lapor->status($id)){
			$this->session->set_flashdata('log', msg('Data berhasil diubah!'));	
		}else{
			$this->session->set_flashdata('log', msg('Data gagal diubah!', 'danger'));	
		}

		redirect('control-panel/lapor');
	}

	public function delete($id){
		if($this->tb_lapor->delete($id)){
			$this->session->set_flashdata('log', msg('Data berhasil dihapus!'));	
		}else{
			$this->session->set_flashdata('log', msg('Data gagal dihapus!', 'danger'));	
		}

		redirect('control-panel/lapor');
	}

}