<?php     
defined('BASEPATH') OR exit('No direct script access allowed');

class Gatra extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		check_access();
		if(get_user()->hak_akses == 1){
			redirect('control-panel');
		}
	}

	public function index(){
		$data = [
			'title' => 'e-Gatra | siBanjar - '.get_roles().' Panel'
		];
		$this->load->view('adminpanel/e_gatra/index',$data);
	}
}