<?php     
defined('BASEPATH') OR exit('No direct script access allowed');

class Lingang extends CI_Controller {
	public function __construct(){
		parent::__construct();
		check_access();

		$this->load->model('tb_lingang');
	}
	
	public function index(){
		if(get_user()->hak_akses == 2){
			$data = [
				'title'   => 'e-Lingang | siBanjar - '.get_roles().' Panel',
				'lingang' => $this->tb_lingang->getAll()
			];

			$this->load->view('adminpanel/e_lingang/index', $data);
		}else if(get_user()->hak_akses == 3){
			$data = [
				'title'   => 'e-Lingang | siBanjar - '.get_roles().' Panel'
			];

			$this->load->view('adminpanel/e_lingang/index2', $data);
		}else{
			show_404();
		}
	}

	public function insert(){
		$data = [
			'title' => 'Tambahkan Data | e-Lingang | siBanjar - '.get_roles().' Panel',
		];

		$rules = [
			[
				'field' => 'nama',
				'label'	=> 'Nama',
				'rules'	=> 'trim|max_length[255]|min_length[3]|xss_clean|required'
			],
			[
				'field' => 'deskripsi',
				'label'	=> 'Deskripsi',
				'rules'	=> 'min_length[3]|xss_clean'
			],
			[
				'field' => 'start',
				'label'	=> 'Tanggal Mulai',
				'rules'	=> 'trim|xss_clean|required'
			],
			[
				'field' => 'start',
				'label'	=> 'Tanggal Berakhir',
				'rules'	=> 'trim|xss_clean|required'
			],
		];

		$this->form_validation->set_rules($rules);

		if($this->form_validation->run()){
			if($this->tb_lingang->store()){
				$this->session->set_flashdata('log', msg('Data berhasil ditambahkan!'));	
			}else{
				$this->session->set_flashdata('log', msg('Data gagal ditambahkan!', 'danger'));	
			}

			redirect('control-panel/lingang/insert');
		}else{
			$this->load->view('adminpanel/e_lingang/insert', $data);
		}
	}

	public function edit($id){
		$data = [
			'title' => 'Edit Data | e-Lingang | siBanjar - '.get_roles().' Panel',
			'data'	=> $this->tb_lingang->getSingle($id)
		];

		$rules = [
			[
				'field' => 'nama',
				'label'	=> 'Nama',
				'rules'	=> 'trim|max_length[255]|min_length[3]|xss_clean|required'
			],
			[
				'field' => 'deskripsi',
				'label'	=> 'Deskripsi',
				'rules'	=> 'min_length[3]|xss_clean'
			],
			[
				'field' => 'start',
				'label'	=> 'Tanggal Mulai',
				'rules'	=> 'trim|xss_clean|required'
			],
			[
				'field' => 'start',
				'label'	=> 'Tanggal Berakhir',
				'rules'	=> 'trim|xss_clean|required'
			],
		];

		$this->form_validation->set_rules($rules);

		if($this->form_validation->run()){
			if($this->tb_lingang->update($id)){
				$this->session->set_flashdata('log', msg('Data berhasil diubah!'));	
			}else{
				$this->session->set_flashdata('log', msg('Data gagal diubah!', 'danger'));	
			}

			redirect('control-panel/lingang');
		}else{
			$this->load->view('adminpanel/e_lingang/edit', $data);
		}
	}

	public function delete($id){
		if($this->tb_lingang->delete($id))
			$this->session->set_flashdata('log', msg('Data berhasil dihapus!'));	
		else
			$this->session->set_flashdata('log', msg('Data gagal dihapus!', 'danger'));	
		
		redirect('control-panel/lingang');
	}
	
	public function event(){
		header('Content-Type: application/json');
		$id = $this->input->get('id');
		if(empty($id)){
			echo json_encode($this->tb_lingang->getAll());
		}else{
			echo json_encode($this->tb_lingang->getSingle($id));
		}
	}

}