<?php     
defined('BASEPATH') OR exit('No direct script access allowed');

class Banjar extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		check_access();

		if(get_user()->hak_akses > 1){
			redirect('control-panel');
		}

		$this->load->model('tb_banjar');
	}

	public function index(){
		$data = [
			'title'  => 'e-Banjar | siBanjar - '.get_roles().' Panel',
			'banjar' => $this->tb_banjar->getAll()
		];

		$this->load->view('adminpanel/e_banjar/index', $data);
	}

	public function insert(){
		$data = [
			'title' => 'Tambahkan Data | e-Banjar | siBanjar - '.get_roles().' Panel',
		];

		$rules = [
			[
				'field' => 'nama',
				'label'	=> 'Nama',
				'rules'	=> 'trim|max_length[255]|min_length[3]|xss_clean|required'
			],
			[
				'field' => 'deskripsi',
				'label'	=> 'Deskripsi',
				'rules'	=> 'min_length[3]|xss_clean'
			],
			[
				'field' => 'alamat',
				'label'	=> 'Alamat',
				'rules'	=> 'required|min_length[3]|xss_clean'
			],
		];

		$this->form_validation->set_rules($rules);

		if($this->form_validation->run()){
			if($this->tb_banjar->store()){
				$this->session->set_flashdata('log', msg('Data berhasil ditambahkan!'));	
			}

			redirect('control-panel/banjar/insert');
		}else{
			$this->load->view('adminpanel/e_banjar/insert', $data);
		}
	}

	public function edit($id){
		$data = [
			'title' => 'Edit Data | e-Banjar | siBanjar - '.get_roles().' Panel',
			'data'  => $this->tb_banjar->getSingle($id)
		];

		$rules = [
			[
				'field' => 'nama',
				'label'	=> 'Nama',
				'rules'	=> 'trim|max_length[255]|min_length[3]|xss_clean|required'
			],
			[
				'field' => 'deskripsi',
				'label'	=> 'Deskripsi',
				'rules'	=> 'min_length[3]|xss_clean'
			],
			[
				'field' => 'alamat',
				'label'	=> 'Alamat',
				'rules'	=> 'required|min_length[3]|xss_clean'
			],
		];

		$this->form_validation->set_rules($rules);

		if($this->form_validation->run()){
			if($this->tb_banjar->update($id)){
				$this->session->set_flashdata('log', msg('Data berhasil diubah!'));	
			}

			redirect('control-panel/banjar');
		}else{
			$this->load->view('adminpanel/e_banjar/edit', $data);
		}
	}

	public function delete($id){
		if($this->tb_banjar->delete($id))
			$this->session->set_flashdata('log', msg('Data berhasil dihapus!'));	
		else
			$this->session->set_flashdata('log', msg('Data gagal dihapus!', 'danger'));	
		
		redirect('control-panel/banjar');

	}
}
