<?php     
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		check_access();

		$this->load->model('tb_banjar');
		$this->load->model('tb_user');
		$this->load->model('tb_lapor');
		$this->load->model('tb_lingang');
	}

	public function index(){
		$data = [
			'title' => 'Dashboard | siBanjar - '.get_roles().' Panel',
			'count' => [
				'banjar' => $this->tb_banjar->count(),
				'penduduk' => $this->tb_user->count()
			]
		];


		if(get_user()->hak_akses == 1)
			$this->load->view('adminpanel/index', $data);
		else if(get_user()->hak_akses == 2){
			$data['count']['laporan'] = $this->tb_lapor->count();
			$data['count']['acara'] = $this->tb_lingang->count();
			$this->load->view('adminpanel/index2', $data);
		}
		else if(get_user()->hak_akses == 3){
			$data['count'] = [];
			$this->load->view('adminpanel/index4', $data);
		}
	}	

	public function profile($id = ''){
		if(get_user()->hak_akses == 1){
			redirect('control-panel');
		}
		if(get_user()->nik == $id){
			$data = [
				'title' => 'Dashboard | siBanjar - '.get_roles().' Panel',
				'data'	=> $this->tb_user->getSingleById($id)
			];

			$rules = [
				[
					'field' => 'pass',
					'label'	=> 'Password',
					'rules'	=> 'trim|max_length[255]|min_length[3]|xss_clean'
				],
				[
					'field' => 'repass',
					'label'	=> 'Password Confirmation',
					'rules'	=> 'trim|max_length[255]|matches[pass]|min_length[3]|xss_clean'
				]
			];

			$this->form_validation->set_rules($rules);

			if($this->form_validation->run()){
				if($this->tb_user->profile($id)){
					$this->session->set_flashdata('log', msg('Data berhasil ditambahkan!'));	
					redirect('logout');
				}else{
					$this->session->set_flashdata('log', msg('Data gagal ditambahkan!', 'danger'));	
					redirect('control-panel/profile/'.$id);
				}

				
			}else{
				$this->load->view('adminpanel/profile', $data);
			}
			
		}else show_404();
	}
}
