<?php     
defined('BASEPATH') OR exit('No direct script access allowed');


class Kaka extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('tb_user');
		$this->load->model('tb_banjar');
		check_access();
	}

	public function index($id = ''){
		if(!empty($id)){
			if(!empty($this->tb_banjar->getSingle($id))){
				$data = [
					'title'  => 'e-Kaka | siBanjar - '.get_roles().' Panel',
					'kaka'   => $this->tb_user->getAllById($id),
					'user'   => $this->tb_user->getAllByBanjar($id),
					'kelian' => $this->tb_user->getKelianByBanjar($id)
				];

				$this->load->view('adminpanel/e_kaka/index', $data);
			}else{
				show_404();
			}
		}else{
			if(get_user()->hak_akses == 1) show_404();

			if(get_user()->hak_akses == 2){
				$data = [
					'title'  => 'e-Kaka | siBanjar - '.get_roles().' Panel',
					'kaka'   => $this->tb_user->getAllById(get_user()->id_banjar),
					'user'   => $this->tb_user->getAllByBanjar(get_user()->id_banjar),
					'kelian' => $this->tb_user->getKelianByBanjar(get_user()->id_banjar)
				];

				$this->load->view('adminpanel/e_kaka/index', $data);
			}else if(get_user()->hak_akses > 2){
				$data = [
					'title'  => 'e-Kaka | siBanjar - '.get_roles().' Panel',
					'kaka'   => $this->tb_user->getSingleByKaka()
				];

				$this->load->view('adminpanel/e_kaka/index', $data);
			}
		}
	}

	public function delete($id){
		if(get_user()->nokk == $id){
			if($this->tb_user->delete($this->input->post('id')))
				$this->session->set_flashdata('log', msg('Data berhasil dihapus!'));	
			else
				$this->session->set_flashdata('log', msg('Data gagal dihapus!', 'danger'));	
		}
		
		redirect('control-panel/kaka/'.$id);
	}

	public function excel($id){
		$this->tb_user->storeExcel($id);
		redirect('control-panel/kaka/'.$id);
	}

	public function kelian($id){
		if(empty(get_user()->id_banjar)) $this->tb_user->storeKelian($id);
		redirect('control-panel/kaka/'.$id);
	}
	
}