<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$GLOBALS['_user'] = [];
$GLOBALS['_ci'] =& get_instance();

if ( ! function_exists('get_user'))
{
	function get_user()
	{	
		if(empty($GLOBALS['_user'])){
			$GLOBALS['_ci']->db->where(['token' => $GLOBALS['_ci']->session->token]);
			$GLOBALS['_user'] = $GLOBALS['_ci']->db->select('tb_user.*, tb_kaka.id_banjar as id_banjar')
												   ->join('tb_kaka', 'tb_kaka.nokk = tb_user.nokk')
												   ->get('tb_user');
			if($GLOBALS['_user']->num_rows() == 1){
				$GLOBALS['_user'] = $GLOBALS['_user']->row();
			}else{
				$GLOBALS['_ci']->db->where(['token' => $GLOBALS['_ci']->session->token]);
				$GLOBALS['_user'] = $GLOBALS['_ci']->db->get('tb_admin')->row();
			}
			return $GLOBALS['_user'];
		}else{
			return $GLOBALS['_user'];
		}
	}   
}

if ( ! function_exists('check_access')){
	function check_access(){	
		$GLOBALS['_ci']=& get_instance();
		if(empty($GLOBALS['_ci']->session->token)) {
			$GLOBALS['_ci']->session->set_flashdata('log', msg('Anda harus login dulu!', 'warning'));	
			redirect('login');
		}
	}   
}

if ( ! function_exists('check_login')){
	function check_login(){	
		$GLOBALS['_ci']=& get_instance();
		if(!empty($GLOBALS['_ci']->session->token)) redirect('control-panel');
	}   
}

if ( ! function_exists('msg')){
	function msg($msg, $type = 'info'){
		return '<div class="alert alert-'.$type.' alert-dismissible" role="alert">'.$msg.' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></div>';
	}
}

if(!function_exists('autoNum')){
	function autoNum($table,$primary,$prefix){
		date_default_timezone_set('Asia/Makassar');
		$GLOBALS['_ci']->db->select('RIGHT('.$primary.',4) as kode', FALSE);
		$GLOBALS['_ci']->db->order_by($primary,'DESC');    
		$GLOBALS['_ci']->db->limit(1);    
		$query = $GLOBALS['_ci']->db->get($table);      //cek dulu apakah ada sudah ada kode di tabel.    
		if($query->num_rows() <> 0){      
			//jika kode ternyata sudah ada.      
			$data = $query->row();      
			$kode = intval($data->kode) + 1;    
		}
		else {      
			//jika kode belum ada      
			$kode = 1;    
		}

		$kodemax = str_pad($kode, 5, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
		$kodejadi = $prefix."-".date('ymd').$kodemax;    
		return $kodejadi;  
	}
}

if(!function_exists('get_roles')){
	function get_roles(){
		$akses = get_user()->hak_akses; 

        if($akses == 1)
            return 'Admin';
        else if($akses == 2)
            return 'Kelian';
        else
            return 'Warga';
	}
}